ASBEGINSET NOCOUNT ON;

-- insertion des actions pour les abonnements créés il y a 2 jours (délai pour les recevoir de SAP via le site)
INSERT INTO ACTION_TI_KARIBOU_SCENARIOS (CREATED_DT, USERID, ACTIONCODE)select DISTINCT GETDATE() AS CREATED_DT,  ID AS USERID, 'CERTI_WELCOME' AS ACTIONCODEfrom users_ti_emails T1 with (nolock)  where	exists (select 1 from DATA_TI_STATS_WEB TA WHERE T1.MAIL_CODE =TA.MAIL_CODE AND TA.PERMISSIONS IN ('ADMINISTRATEUR','ADMINISTRATEUR_DELEGUE') ) 	and exists (select 1 from DATA_TI_ABO TB WHERE T1.MAIL_CODE =TB.MAIL_CODE AND TB.OFFER_TEMPLATE IN ('TITKARFREE','TITKARWEB') AND TB.STATUT_ABONNEMENT=1 AND CERTI_JETONS=0 AND DEBUT_ABONNEMENT > GETDATE()-3)	and not exists (select 1 from ACTION_TI_KARIBOU_SCENARIOS TA with (nolock)  where ACTIONCODE='CERTI_WELCOME' AND CREATED_DT > DATEADD(dd,-60,GETDATE()) AND T1.ID = TA.USERID );
	
	
	END
AS
BEGIN
SET NOCOUNT ON;

-------------------------------------------------------------------------------------------
-------------------------------------------------------------------------------------------
------------------------------------ CERTI 1 : WELCOME ------------------------------------
-------------------------------------------------------------------------------------------
-------------------------------------------------------------------------------------------


-- CAS POUR Les sorties D&Q
 INSERT INTO ACTION_TI_KARIBOU_SCENARIOS (USERID,CREATED_DT,ACTIONCODE,ID_SCENARIO,IMAGE_THEME,ACCROCHE_THEME,ARTICLES_THEME,ACCROCHE_TOP5,DOMAINE,IMAGE_DOMAINE,ACCROCHE_DOMAINE,CAMPAGNE_DQ,EXCLURE_ABOS_EN_COURS,ARTICLES_TOP5,THEME)
SELECT T1.ID, GETDATE(),'CERTI_1',T3.ID,IMAGE_THEME,ACCROCHE_THEME,ARTICLES_THEME,ACCROCHE_TOP5,DOMAINE,IMAGE_DOMAINE,ACCROCHE_DOMAINE,CAMPAGNE_DQ,EXCLURE_ABOS_EN_COURS,ARTICLES_TOP5,TREATY_LABEL AS THEME  from USERS_TI_EMAILS T1 
INNER JOIN ACTION_TI_KARIBOU_DOC_QUIZZ TA ON TA.USERID=T1.ID AND state=30 and cast(exec_dt as date)= cast(getdate()-7 as date) and actioncode LIKE 'R_%'
INNER JOIN ARTICLES_TI_KARIBOU_SCENARIOS_REF T3  with (nolock) ON T3.ID = TA.ID_SCENARIO
WHERE
T1.dormeur is null and optout is null
-- exclusion des abos TITKARWEB et TITKARFREE
and not exists (select 1 from DATA_TI_ABO TC WHERE TC.MAIL_CODE=T1.ID AND TC.STATUT_ABONNEMENT=1 AND TC.OFFER_TEMPLATE IN ('TITKARWEB','TITKARFREE'))
-- Exclure les DI en cours assignées à Kristel dont la date de création est supérieure date de création > J-3 mois
and not exists (select 1 from DATA_TI_DI TD WHERE T1.ID = TD.MAIL_CODE and TD.DATE_DEMANDE > getdate()-90 and status='todo' and ASSIGNED_TO = 'kdonjon') 
-- Exclure les DI terminées assignées à Kristel dont la date de clôture est supérieure date de clôture > J-3 jours
and not exists (select 1 from DATA_TI_DI TE WHERE T1.ID = TE.MAIL_CODE and TE.DATE_DEMANDE > getdate()-90 and status='todo' and ASSIGNED_TO = 'kdonjon') 
-- Exclure RV terminé J-7 ou 
--Exclure RV planifié -7 jours / + 30 jours
and not exists (select 1 from DATA_TI_ACTIVITY TF where T1.ID=TF.MAIL_CODE AND ((status='Held' and date_end > getdate()-7) or (status='Planned' and date_start between getdate()-7 and getdate()+30)) )
-- Exclure les affaires/devis dont la date de création dans les 3 derniers mois
and not exists (select 1 from Data_TI_LIGNE_CDE TG WHERE TG.MAIL_CODE = T1.MAIL_CODE AND TG.DATE_COMMANDE >getdate()-90) 
-- exclusion de ceux a qui on a déjà envoyé l'email dans les 90 derniers jours 
and not exists (select 1 from ACTION_TI_KARIBOU_SCENARIOS TG where TG.USERID=T1.ID AND TG.ACTIONCODE='CERTI_1' AND CREATED_DT > getdate()-90)
-------------------------------------------------------------------------------------------

 
-- CAS POUR Les sorties DI
INSERT INTO ACTION_TI_KARIBOU_SCENARIOS (USERID,CREATED_DT,ACTIONCODE,ID_SCENARIO,IMAGE_THEME,ACCROCHE_THEME,ARTICLES_THEME,ACCROCHE_TOP5,DOMAINE,IMAGE_DOMAINE,ACCROCHE_DOMAINE,CAMPAGNE_DQ,EXCLURE_ABOS_EN_COURS,ARTICLES_TOP5,THEME)
SELECT T1.ID, GETDATE(),'CERTI_1',T3.ID,IMAGE_THEME,ACCROCHE_THEME,ARTICLES_THEME,ACCROCHE_TOP5,DOMAINE,IMAGE_DOMAINE,ACCROCHE_DOMAINE,CAMPAGNE_DQ,EXCLURE_ABOS_EN_COURS,ARTICLES_TOP5,TREATY_LABEL AS THEME  
from USERS_TI_EMAILS T1 
INNER JOIN  DATA_TI_DI T2 ON T1.ID = T2.MAIL_CODE AND T2.TYPE='ti_mkg_demo' and T2.DATE_TRAITEMENT=cast(getdate()-3 as date)
INNER JOIN ARTICLES_TI_KARIBOU_SCENARIOS_REF T3  with (nolock) ON T3.SKU_PRODUIT = T2.SKU
WHERE T1.dormeur is null and optout is null
-- exclusion des abos TITKARWEB et TITKARFREE
and not exists (select 1 from DATA_TI_ABO TC WHERE TC.MAIL_CODE=T1.ID AND TC.STATUT_ABONNEMENT=1 AND TC.OFFER_TEMPLATE IN ('TITKARWEB','TITKARFREE'))
-- Exclure les DI en cours assignées à Kristel dont la date de création est supérieure date de création > J-3 mois
and not exists (select 1 from DATA_TI_DI TD WHERE T1.ID = TD.MAIL_CODE and TD.DATE_DEMANDE > getdate()-90 and status='todo' and ASSIGNED_TO = 'kdonjon') 
-- Exclure les DI terminées assignées à Kristel dont la date de clôture est supérieure date de clôture > J-3 jours
and not exists (select 1 from DATA_TI_DI TE WHERE T1.ID = TE.MAIL_CODE and TE.DATE_DEMANDE > getdate()-90 and status='todo' and ASSIGNED_TO = 'kdonjon') 
-- Exclure RV terminé J-7 ou 
--Exclure RV planifié -7 jours / + 30 jours
and not exists (select 1 from DATA_TI_ACTIVITY TF where T1.ID=TF.MAIL_CODE AND ((status='Held' and date_end > getdate()-7) or (status='Planned' and date_start between getdate()-7 and getdate()+30)) )
-- Exclure les affaires/devis dont la date de création dans les 3 derniers mois
and not exists (select 1 from Data_TI_LIGNE_CDE TG WHERE TG.MAIL_CODE = T1.MAIL_CODE AND TG.DATE_COMMANDE >getdate()-90) 
-- exclusion de ceux a qui on a déjà envoyé l'email dans les 90 derniers jours 
and not exists (select 1 from ACTION_TI_KARIBOU_SCENARIOS TG where TG.USERID=T1.ID AND TG.ACTIONCODE='CERTI_1' AND CREATED_DT > getdate()-90)




-------------------------------------------------------------------------------------------
-------------------------------------------------------------------------------------------
------------------------------------ CERTI 2 : OFFRE CERTI	-------------------------------
-------------------------------------------------------------------------------------------
-------------------------------------------------------------------------------------------

INSERT INTO ACTION_TI_KARIBOU_SCENARIOS (USERID,CREATED_DT,ACTIONCODE,ID_SCENARIO,IMAGE_THEME,ACCROCHE_THEME,ARTICLES_THEME,ACCROCHE_TOP5,DOMAINE,IMAGE_DOMAINE,ACCROCHE_DOMAINE,CAMPAGNE_DQ,EXCLURE_ABOS_EN_COURS,ARTICLES_TOP5,THEME)
select 
	T1.USERID, 
	GETDATE(),
	'CERTI_2',
	T3.ID,
	T3.IMAGE_THEME,
	T3.ACCROCHE_THEME,
	T3.ARTICLES_THEME,
	T3.ACCROCHE_TOP5,
	T3.DOMAINE,
	T3.IMAGE_DOMAINE,
	T3.ACCROCHE_DOMAINE,
	T3.CAMPAGNE_DQ,
	T3.EXCLURE_ABOS_EN_COURS,
	T3.ARTICLES_TOP5,TREATY_LABEL AS THEME
from ACTION_TI_KARIBOU_SCENARIOS T1
INNER JOIN ARTICLES_TI_KARIBOU_SCENARIOS_REF T3  with (nolock) ON T3.ID = T1.ID_SCENARIO
where 
T1.actioncode = 'CERTI_1' AND state=30 and cast(exec_dt as date)= cast(getdate()-9 as date) -- /!\ /!\ /!\ /!\ /!\ selection de du premier email il y a 9 jours/!\ /!\ /!\ /!\ /!\ 
-- exclusion des abonnés en cours 
and not exists (select 1 from DATA_TI_STATS_WEB TA where T1.USERID=TA.MAIL_CODE AND SUBSCRIBER_TYPE='ABONNE_EN_COURS') 
-- exclusion des devis en cours
and not exists (select 1 from Data_TI_LIGNE_CDE TB where T1.USERID=TB.MAIL_CODE AND  SALES_STAGE NOT IN ('Closed Lost','Closed Won')) 
-- Exclure les DI en cours assignées à Kristel dont la date de création est supérieure date de création > J-3 mois
and not exists (select 1 from DATA_TI_DI TD WHERE T1.USERID = TD.MAIL_CODE and TD.DATE_DEMANDE > getdate()-90 and status='todo' and ASSIGNED_TO = 'kdonjon') 
-- Exclure les DI terminées assignées à Kristel dont la date de clôture est supérieure date de clôture > J-3 jours
and not exists (select 1 from DATA_TI_DI TE WHERE T1.USERID = TE.MAIL_CODE and TE.DATE_DEMANDE > getdate()-90 and status='todo' and ASSIGNED_TO = 'kdonjon') 
-- Exclure RV terminé J-7 ou 
--Exclure RV planifié -7 jours / + 30 jours
and not exists (select 1 from DATA_TI_ACTIVITY TF where T1.USERID=TF.MAIL_CODE AND ((status='Held' and date_end > getdate()-7) or (status='Planned' and date_start between getdate()-7 and getdate()+30)) )
-- Exclure les affaires/devis dont la date de création dans les 3 derniers mois
and not exists (select 1 from Data_TI_LIGNE_CDE TG WHERE TG.MAIL_CODE = T1.USERID AND TG.DATE_COMMANDE >getdate()-90) 



-------------------------------------------------------------------------------------------
-------------------------------------------------------------------------------------------
------------------------------------ CERTI 3 : TOP 5	-------------------------------
-------------------------------------------------------------------------------------------
-------------------------------------------------------------------------------------------

INSERT INTO ACTION_TI_KARIBOU_SCENARIOS (USERID,CREATED_DT,ACTIONCODE,ID_SCENARIO,IMAGE_THEME,ACCROCHE_THEME,ARTICLES_THEME,ACCROCHE_TOP5,DOMAINE,IMAGE_DOMAINE,ACCROCHE_DOMAINE,CAMPAGNE_DQ,EXCLURE_ABOS_EN_COURS,ARTICLES_TOP5,THEME)
select 
	T1.USERID, 
	GETDATE(),
	'CERTI_3',
	T3.ID,
	T3.IMAGE_THEME,
	T3.ACCROCHE_THEME,
	T3.ARTICLES_THEME,
	T3.ACCROCHE_TOP5,
	T3.DOMAINE,
	T3.IMAGE_DOMAINE,
	T3.ACCROCHE_DOMAINE,
	T3.CAMPAGNE_DQ,
	T3.EXCLURE_ABOS_EN_COURS,
	T3.ARTICLES_TOP5,TREATY_LABEL AS THEME
from ACTION_TI_KARIBOU_SCENARIOS T1
INNER JOIN ARTICLES_TI_KARIBOU_SCENARIOS_REF T3  with (nolock) ON T3.ID = T1.ID_SCENARIO
where 
T1.actioncode = 'CERTI_2' AND state=30 and cast(exec_dt as date)= cast(getdate()-9 as date) -- /!\ /!\ /!\ /!\ /!\ selection de du premier email il y a 9 jours/!\ /!\ /!\ /!\ /!\ 
-- exclusion des abonnés en cours 
and not exists (select 1 from DATA_TI_STATS_WEB TA where T1.USERID=TA.MAIL_CODE AND SUBSCRIBER_TYPE='ABONNE_EN_COURS') 
-- exclusion des devis en cours
and not exists (select 1 from Data_TI_LIGNE_CDE TB where T1.USERID=TB.MAIL_CODE AND  SALES_STAGE NOT IN ('Closed Lost','Closed Won')) 
-- Exclure les DI en cours assignées à Kristel dont la date de création est supérieure date de création > J-3 mois
and not exists (select 1 from DATA_TI_DI TD WHERE T1.USERID = TD.MAIL_CODE and TD.DATE_DEMANDE > getdate()-90 and status='todo' and ASSIGNED_TO = 'kdonjon') 
-- Exclure les DI terminées assignées à Kristel dont la date de clôture est supérieure date de clôture > J-3 jours
and not exists (select 1 from DATA_TI_DI TE WHERE T1.USERID = TE.MAIL_CODE and TE.DATE_DEMANDE > getdate()-90 and status='todo' and ASSIGNED_TO = 'kdonjon') 
-- Exclure RV terminé J-7 ou 
--Exclure RV planifié -7 jours / + 30 jours
and not exists (select 1 from DATA_TI_ACTIVITY TF where T1.USERID=TF.MAIL_CODE AND ((status='Held' and date_end > getdate()-7) or (status='Planned' and date_start between getdate()-7 and getdate()+30)) )
-- Exclure les affaires/devis dont la date de création dans les 3 derniers mois
and not exists (select 1 from Data_TI_LIGNE_CDE TG WHERE TG.MAIL_CODE = T1.USERID AND TG.DATE_COMMANDE >getdate()-90) 




-------------------------------------------------------------------------------------------
-------------------------------------------------------------------------------------------
------------------------------------ CERTI 4 : Demo Certi	-------------------------------
-------------------------------------------------------------------------------------------
-------------------------------------------------------------------------------------------
INSERT INTO ACTION_TI_KARIBOU_SCENARIOS (USERID,CREATED_DT,ACTIONCODE,ID_SCENARIO,IMAGE_THEME,ACCROCHE_THEME,ARTICLES_THEME,ACCROCHE_TOP5,DOMAINE,IMAGE_DOMAINE,ACCROCHE_DOMAINE,CAMPAGNE_DQ,EXCLURE_ABOS_EN_COURS,ARTICLES_TOP5,THEME)
select 
	T1.USERID, 
	GETDATE(),
	'CERTI_4',
	T3.ID,
	T3.IMAGE_THEME,
	T3.ACCROCHE_THEME,
	T3.ARTICLES_THEME,
	T3.ACCROCHE_TOP5,
	T3.DOMAINE,
	T3.IMAGE_DOMAINE,
	T3.ACCROCHE_DOMAINE,
	T3.CAMPAGNE_DQ,
	T3.EXCLURE_ABOS_EN_COURS,
	T3.ARTICLES_TOP5,TREATY_LABEL AS THEME
from ACTION_TI_KARIBOU_SCENARIOS T1
INNER JOIN ARTICLES_TI_KARIBOU_SCENARIOS_REF T3  with (nolock) ON T3.ID = T1.ID_SCENARIO
where 
T1.actioncode = 'CERTI_3' AND state=30 and cast(exec_dt as date)= cast(getdate()-7 as date) -- /!\ /!\ /!\ /!\ /!\ selection de du premier email il y a 7 jours/!\ /!\ /!\ /!\ /!\ 
-- exclusion des abos TITKARWEB et TITKARFREE
and not exists (select 1 from DATA_TI_ABO TC WHERE TC.MAIL_CODE=T1.ID AND TC.STATUT_ABONNEMENT=1 AND TC.OFFER_TEMPLATE IN ('TITKARWEB','TITKARFREE'))
-- exclusion de ceux a qui on a déjà envoyé l'email dans les 90 derniers jours 
and not exists (select 1 from ACTION_TI_KARIBOU_SCENARIOS TG where TG.USERID=T1.ID AND TG.ACTIONCODE='CERTI_4' AND CREATED_DT > getdate()-180)


-------------------------------------------------------------------------------------------
-------------------------------------------------------------------------------------------
------------------------------------ CERTI 5 : Essai gratuit Certi	-----------------------
-------------------------------------------------------------------------------------------
-------------------------------------------------------------------------------------------
INSERT INTO ACTION_TI_KARIBOU_SCENARIOS (USERID,CREATED_DT,ACTIONCODE)
select 
	T1.ID, 
	GETDATE(),
	'CERTI_5'
from USERS_TI_EMAILS T1
where 
-- Envoyé aux multicliqueurs (“récidivistes” = a cliqué sur 3 liens différents de tous les emails Top5 dans les trente derniers jours  (?) (sauf liens de désabo), quelque soit le thème => réglage du paramètre quantité à affiner, 3 au lancement)
exists (select 1 from flags with (nolock) 
				where campaignid=6302 and probeid>0
				and DT > getdate()-30
				and userid=T1.ID
				group by userid
				having count(distinct(concat(probeid,DITEMID))) > 2)

-- exclusion des abos TITKARWEB et TITKARFREE
and not exists (select 1 from DATA_TI_ABO TC WHERE TC.MAIL_CODE=T1.ID AND TC.STATUT_ABONNEMENT=1 AND TC.OFFER_TEMPLATE IN ('TITKARWEB','TITKARFREE'))
-- Exclure tous ceux qui ont eu des droits sur TITKARFREE les 3 derniers mois et qui sont pays FR CH BE LU MO dont la date de début abonnement > J-90 mois 
And not exists (
	select 1 from DATA_TI_ABO TC WHERE TC.MAIL_CODE=T1.ID AND TC.DEBUT_ABONNEMENT> GETDATE()-90 AND TC.OFFER_TEMPLATE IN ('TITKARWEB','TITKARFREE') 
	and (
		-- exclusion des suspects en FR,CH,BE,LU,MO
		exists(select 1 from  DATA_TI_SUS_INFOSPEC TX WHERE TX.MAIL_CODE = T1.ID and ADDRESS_COUNTRY IN ('BE','CH','FR','LU','MC') )
		or 
		-- exclusion des comptes
		exists (select 1 from DATA_TI_PM_ADRPRI TZ WHERE TZ.COMPTE_ID = T1.COMPTE_ID and ADDRESS_COUNTRY IN ('BE','CH','FR','LU','MC') )
		)
) 
-- Exclure tous ceux qui ont eu des droits sur TITKARFREE l’année et qui sont hors pays FR CH BE LU MO (inclus les pays vide) dans la date de début abonnement > J-365 mois 
And not exists (
	select 1 from DATA_TI_ABO TC WHERE TC.MAIL_CODE=T1.ID AND TC.DEBUT_ABONNEMENT> GETDATE()-365 AND TC.OFFER_TEMPLATE IN ('TITKARWEB','TITKARFREE') 
	and (
		-- exclusion des suspects en FR,CH,BE,LU,MO
		exists(select 1 from  DATA_TI_SUS_INFOSPEC TX WHERE TX.MAIL_CODE = T1.ID and (ADDRESS_COUNTRY NOT IN ('BE','CH','FR','LU','MC') or ADDRESS_COUNTRY IS NULL )  )
		or 
		-- exclusion des comptes
		exists (select 1 from DATA_TI_PM_ADRPRI TZ WHERE TZ.COMPTE_ID = T1.COMPTE_ID and (ADDRESS_COUNTRY NOT IN ('BE','CH','FR','LU','MC') or ADDRESS_COUNTRY IS NULL ) )
		)
) 
-- Exclure les étudiants
And not exists (select 1 from DATA_TI_STATS_WEB TA WHERE TA.MAIL_CODE=T1.ID and profil='STUDENT')
--exclusion de ceux ayant déjà reçu dans les 90 derniers jours 				
and not exists (select 1 from ACTION_TI_KARIBOU_SCENARIOS TG where TG.USERID=T1.ID AND TG.ACTIONCODE='CERTI_5' AND CREATED_DT > getdate()-90)



END
-- gestion de l'univers TI 
UPDATE T1 
SET UNIVERSTI=1
from users_ti_emails T1
where (T1.universTI=0 or T1.universTI is null)
AND (
	exists (select 1 from DATA_TI_DOWNLOAD T2 with (nolock) WHERE T1.ID = T2.MAIL_CODE AND T2.TYPE_ACTIVITE not like 'ti_phenix%'  AND T2.TYPE_ACTIVITE not like 'ti_quick%' )
	or
	exists (select 1 from DATA_TI_DI T2 with (nolock) WHERE T1.ID = T2.MAIL_CODE AND T2.TYPE not like 'ti_phenix%'  AND T2.TYPE not like 'ti_quick%' )  
	or
	exists (select 1 from ACTION_TI_EMT T2 with (nolock) WHERE T1.ID = T2.USERID)  
	or 
	exists (select 1 from DATA_TI_ABO T2 with (nolock) WHERE T1.ID = T2.MAIL_CODE)  
)


-- gestion de l'univers KreaCCTP 
UPDATE T1 
SET UNIVERSKREACCTP=1
from users_ti_emails T1
where (T1.UNIVERSKREACCTP=0 or T1.UNIVERSKREACCTP is null)
AND (
	exists (select 1 from DATA_TI_DOWNLOAD T2 with (nolock) WHERE T1.ID = T2.MAIL_CODE AND T2.TYPE_ACTIVITE like 'ti_phenix%')
	or
	exists (select 1 from DATA_TI_DI T2 with (nolock) WHERE T1.ID = T2.MAIL_CODE AND T2.TYPE like 'ti_phenix%' )  
	or
	exists (select 1 from ACTION_TI_KREACCTP_EMT T2 with (nolock) WHERE T1.ID = T2.USERID)  
	or 
	exists (select 1 from DATA_TI_KREACCTP_ABOS T2 with (nolock) WHERE T1.ID = T2.MAIL_CODE)  
	or 
	exists (select 1 from DATA_TI_KREACCTP_PROFIL T2 with (nolock) WHERE T1.ID = T2.MAIL_CODE)  
)


-- gestion de l'univers Quick 
UPDATE T1 
SET UNIVERSQUICK=1
from users_ti_emails T1
where (T1.UNIVERSQUICK=0 or T1.UNIVERSQUICK is null)
AND (
	exists (select 1 from DATA_TI_DOWNLOAD T2 with (nolock) WHERE T1.ID = T2.MAIL_CODE AND T2.TYPE_ACTIVITE like 'ti_quick%')
	or
	exists (select 1 from DATA_TI_DI T2 with (nolock) WHERE T1.ID = T2.MAIL_CODE AND T2.TYPE like 'ti_quick%' )  
)

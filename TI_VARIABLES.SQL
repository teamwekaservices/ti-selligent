-- Permet de charger dans le système les variables définies dans la table 
-- fusion des articles avec le statut publish (statut = 2) 
MERGE ENVIRONMENT_VARIABLES AS T
USING (SELECT 
			VALEUR AS VALUE,
			NOM_VARIABLE_SELLIGENT AS NAME
		FROM DATA_TI_VARIABLES) AS S
ON (T.NAME = S.NAME) 
WHEN NOT MATCHED BY TARGET
	THEN 
		INSERT (CREATED_DT,
			MODIFIED_DT,
			NAME,
			VALUE,
			OWNERID
			)
		VALUES(GETDATE(),
			GETDATE(),
			S.NAME,
			S.VALUE,
			4) 
WHEN MATCHED
	THEN 
		UPDATE SET T.MODIFIED_DT=GETDATE(),
			T.VALUE=S.VALUE;
			
			
			
-- suppression des variables qui appartiennent à TI et qui ne sont pas présentes dans la table TI_VARIABLES
DELETE FROM ENVIRONMENT_VARIABLES
WHERE NOT EXISTS (SELECT 1 FROM DATA_TI_VARIABLES WHERE NOM_VARIABLE_SELLIGENT = NAME)
AND NAME LIKE 'SYSTEM.ENV_TI_%';


-- Mise à jour des mois 
UPDATE  ENVIRONMENT_VARIABLES
SET VALUE = (SELECT 
CASE MONTH(GETDATE())
	WHEN 1 THEN 'janvier'
	WHEN 2 THEN 'février'
	WHEN 3 THEN 'mars'
	WHEN 4 THEN 'avril'
	WHEN 5 THEN 'mai'
	WHEN 6 THEN 'juin'
	WHEN 7 THEN 'juillet'
	WHEN 8 THEN 'aout'
	WHEN 9 THEN 'septembre'
	WHEN 10 THEN 'octobre'
	WHEN 11 THEN 'novembre'
	WHEN 12 THEN 'décembre'
END AS [Mois_En_Cours])
WHERE NAME='SYSTEM.ENV_MOIS_FR'
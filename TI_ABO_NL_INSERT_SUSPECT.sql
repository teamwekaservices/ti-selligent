AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;


INSERT INTO DBO.DATA_TI_NL (SUSPECT_ID,NEWSLETTER,DATE_INSCRIPTION,STATUT_ABONNEMENT)
SELECT 
	SUSPECT_ID,
	TITRE_NEWSLETTER,
	DATE_INSCRIPTION,
	STATUT_ABONNEMENT
FROM dbo.TMP_TI_NL_ABO_SUSPECT_INSERT;
;


END
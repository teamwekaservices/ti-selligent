@DELTADAYS INT

AS
BEGIN
SET NOCOUNT ON


--1 somme/campagne
	MERGE INTO DATA_TI_STATS_DATASTUDIO AS T
	USING (
	SELECT T1.CAMPAIGNID, SUM(VIEWCOUNT) AS VIEWCOUNT,sum(UVIEWCOUNT) as UVIEWCOUNT, sum(TARGETCOUNT) as TARGETCOUNT, sum(DELIVERYCOUNT) as DELIVERYCOUNT, sum(BOUNCECOUNT) as BOUNCECOUNT,
	T2.NAME, sum(CLICKCOUNT) as CLICKCOUNT, sum(UCLICKCOUNT) as UCLICKCOUNT, MAX(RUN_DT) AS RUN_DT,T2.CATEGORY, T4.NAME AS NOM_MAIL,T4.ID AS MAILID
	FROM sim_reporting_flowmetrics T1 WITH (NOLOCK)
	INNER JOIN CAMPAIGNS T2 WITH (NOLOCK) ON T1.CAMPAIGNID=T2.ID AND T2.CATEGORY LIKE 'TI_MKG_SCENARIOS' AND T2.RUN_DT >= GETDATE()-@DELTADAYS
	INNER JOIN CAMPAIGN_ACTIONS T3 with (nolock) on T1.campaignid=t3.campaignid and T1.actionid=t3.actionid
	INNER JOIN MAILS T4 with (nolock) on T3.mailid=T4.id
group by T1.CAMPAIGNID,T2.CATEGORY, T4.NAME,T4.ID,T2.NAME
	) AS S
	ON (T.CAMPAIGNID=S.CAMPAIGNID AND T.MAILID=S.MAILID AND T.NOM_STAT='TI_MKG_SCENARIOS_CUMUL')
	WHEN NOT MATCHED BY TARGET
		THEN
		INSERT (UPDATE_DT,CAMPAIGNID,NB_OUVERTURES,NOM_STAT,NB_U_OUVERTURES,NB_CIBLE,NB_DELIVRE,NB_BOUNCES,NOM_CAMPAGNE,NB_CLICS,NB_U_CLICS,DT,NOM_MAIL,MAILID)
			 VALUES (getdate(),S.CAMPAIGNID,S.VIEWCOUNT,'TI_MKG_SCENARIOS_CUMUL',S.UVIEWCOUNT,S.TARGETCOUNT,S.DELIVERYCOUNT,S.BOUNCECOUNT,
	S.NAME,S.CLICKCOUNT,S.UCLICKCOUNT,S.RUN_DT,S.NOM_MAIL,S.MAILID)
		WHEN MATCHED
			THEN
				UPDATE SET
					T.UPDATE_DT=getdate(),
					T.NB_OUVERTURES=S.VIEWCOUNT,
					T.NB_U_OUVERTURES=S.UVIEWCOUNT,
					T.NB_CIBLE=S.TARGETCOUNT,
					T.NB_DELIVRE=S.DELIVERYCOUNT,
					T.NB_BOUNCES=S.BOUNCECOUNT,
					T.NOM_CAMPAGNE=S.NAME,
					T.NB_CLICS=S.CLICKCOUNT,
					T.NB_U_CLICS=S.UCLICKCOUNT,
					T.DT=S.RUN_DT,
					T.NOM_MAIL=S.NOM_MAIL;

-- 1 bis maj des taux de désabo
MERGE INTO DATA_TI_STATS_DATASTUDIO AS T
USING (
	select t1.campaignid,t2.mailid,count(distinct(userid)) AS NB_U_OPTOUT,  count(userid) AS NB_OPTOUT
FROM flags t1 with (nolock)
	inner join CAMPAIGN_ACTIONS t2 with (nolock) on t1.campaignid=t2.campaignid and t1.actionid=t2.actionid
	and t1.probeid>0
	where exists (select 1 from campaigns with (nolock) where CAMPAIGNS.CATEGORY = 'TI_MKG_SCENARIOS' AND t1.campaignid=campaigns.id and campaigns.run_dt>=cast(getdate()-(@DELTADAYS+7) as date))
	and exists (select 1 from MAIL_PROBES t3 with (nolock) where t2.mailid=t3.mailid and t1.probeid=t3.probeid AND t3.CATEGORY ='OPTOUT')
	group by t1.campaignid,t2.mailid
) AS S
ON (T.CAMPAIGNID=S.CAMPAIGNID  AND T.MAILID=S.MAILID AND T.NOM_STAT='TI_MKG_SCENARIOS_CUMUL') 
WHEN NOT MATCHED BY TARGET
	THEN 
	INSERT (campaignid,mailid,NB_OPTOUT,NB_U_OPTOUT,nom_stat,update_dt)
		 VALUES (S.CAMPAIGNID,S.mailid,S.NB_OPTOUT,S.NB_U_OPTOUT,'TI_MKG_SCENARIOS_CUMUL',getdate())
	WHEN MATCHED
		THEN 
			UPDATE SET 
		t.NB_OPTOUT=S.NB_OPTOUT,
		t.NB_U_OPTOUT=S.NB_U_OPTOUT,
		t.update_dt=getdate();

-- 2 NB_DELIVRE, NB_BOUNCES, NB_ENVOYE total/campagne/date
	MERGE DATA_TI_STATS_DATASTUDIO AS T
	USING (select 
		cast(sent_dt as date) as DT,
		campaignid,
		T2.NAME AS NOM_CAMPAGNE,
		T3.NAME AS NOM_MAIL,
		T3.ID AS MAILID,
		sum(case when smtperr=250 then 1 else 0 end) as NB_DELIVRE,
		sum(case when smtperr=250 then 0 else 1 end) as NB_BOUNCE,
		count(*) as NB_ENVOYE
	from ACTIONQUEUE T1 with (nolock)
	INNER JOIN campaigns T2 with (nolock) ON T2.id=T1.campaignid and T2.category='TI_MKG_SCENARIOS' AND T2.RUN_DT > cast(getdate()-(@DELTADAYS+7) as date)
	INNER JOIN MAILS T3 with (nolock) ON T3.ID = T1.MAILID
	WHERE 
	T1.sent_dt  >= cast(getdate()-(@DELTADAYS+7) as date)
	group by cast(sent_dt as date),campaignid,T2.NAME,T3.NAME,T3.ID ) AS S
	ON (T.DT=S.DT AND T.CAMPAIGNID=S.CAMPAIGNID AND T.MAILID=S.MAILID AND T.NOM_STAT='TI_MKG_SCENARIOS')
	WHEN NOT MATCHED BY TARGET
		THEN
		INSERT (NOM_STAT
			,[DT]
			,CAMPAIGNID
			,[NB_DELIVRE]
			,[NB_BOUNCES]
			,NB_CIBLE
			,NOM_CAMPAGNE
			,NOM_MAIL
			,MAILID
			,UPDATE_DT)
			 VALUES ('TI_MKG_SCENARIOS'
				   ,S.[DT]
				   ,S.[CAMPAIGNID]
				   ,S.[NB_DELIVRE]
				   ,S.NB_BOUNCE
				   ,S.NB_ENVOYE
				,S.NOM_CAMPAGNE
				,S.NOM_MAIL
				,S.MAILID
				,getdate())
		WHEN MATCHED
			THEN
				UPDATE SET
				T.UPDATE_DT = getdate()
				   ,T.[NB_DELIVRE]=S.NB_DELIVRE
				   ,T.[NB_BOUNCES]=S.NB_BOUNCE
				   ,T.NB_CIBLE =S.NB_ENVOYE
				,T.NOM_CAMPAGNE=S.NOM_CAMPAGNE
				,T.NOM_MAIL=S.NOM_MAIL
				,T.MAILID=S.MAILID;


-- complétion des taux d'ouverture
drop table if exists #TMP_TI_STATS_OPEN;

CREATE TABLE #TMP_TI_STATS_OPEN (
DT DATE, 
CAMPAIGNID INT,
MAILID INT,
NB_OUVERTURE INT,
NB_U_OUVERTURE INT
)
INSERT INTO #TMP_TI_STATS_OPEN (DT,CAMPAIGNID,MAILID,NB_OUVERTURE,NB_U_OUVERTURE)
select 
	cast(T3.sent_dt as date) as DT,
	T1.campaignid, 
	T3.MAILID,
	count(*) as NB_OUVERTURE, 
	count(distinct(T1.userid)) as NB_U_OUVERTURE 
from flags T1 with (nolock)
inner join campaigns T2 with (nolock) ON T2.id=T1.campaignid and T2.category='TI_MKG_SCENARIOS' AND T2.run_dt  >= cast(getdate()-(@DELTADAYS+7) as date) 
inner join actionqueue T3 with (nolock) ON T1.inqueueid=T3.inqueueid and T1.CAMPAIGNID=T3.CAMPAIGNID AND T3.sent_dt  >= cast(getdate()-(@DELTADAYS+7) as date)
WHERE 
T1.DT  >= cast(getdate()-(@DELTADAYS+7) as date)
AND  T1.probeid=-1 
group by cast(T3.sent_dt as date),T1.campaignid,T3.MAILID

UPDATE T2
SET 
T2.NB_OUVERTURES = T1.NB_OUVERTURE ,
T2.NB_U_OUVERTURES = T1.NB_U_OUVERTURE
from #TMP_TI_STATS_OPEN T1
inner join DATA_TI_STATS_DATASTUDIO T2 ON T1.CAMPAIGNID = T2.CAMPAIGNID AND T2.MAILID=T1.MAILID AND T2.DT = T1.DT AND T2.NOM_STAT='TI_MKG_SCENARIOS'


drop table if exists #TMP_TI_STATS_OPEN;


-- complétion des taux de clics 

drop table if exists #TMP_TI_STATS_CLIC;

CREATE TABLE #TMP_TI_STATS_CLIC (
DT DATE, 
CAMPAIGNID INT,
MAILID INT,
NB_CLIC INT,
NB_U_CLIC INT
)
INSERT INTO #TMP_TI_STATS_CLIC (DT,CAMPAIGNID,MAILID,NB_CLIC,NB_U_CLIC)
select 
	cast(T3.sent_dt as date) as DT,
	T1.campaignid, 
	T3.MAILID,
	count(*) as NB_CLIC, 
	count(distinct(T1.userid)) as NB_U_CLIC 
from flags T1 with (nolock)
inner join campaigns T2 with (nolock) ON T2.id=T1.campaignid and T2.category='TI_MKG_SCENARIOS' AND T2.run_dt  >=cast(getdate()-(@DELTADAYS+7) as date)
inner join actionqueue T3 with (nolock) ON T1.inqueueid=T3.inqueueid and T1.CAMPAIGNID=T3.CAMPAIGNID AND T3.sent_dt  >= cast(getdate()-(@DELTADAYS+7) as date)
WHERE 
T1.DT  >= cast(getdate()-(@DELTADAYS+7) as date)
	and probeid>200
group by cast(T3.sent_dt as date),T1.campaignid,T3.MAILID


UPDATE T2
SET 
T2.NB_CLICS = T1.NB_CLIC ,
T2.NB_U_CLICS = T1.NB_U_CLIC
from #TMP_TI_STATS_CLIC T1
inner join DATA_TI_STATS_DATASTUDIO T2 ON T1.CAMPAIGNID = T2.CAMPAIGNID AND T2.MAILID=T1.MAILID AND T2.DT = T1.DT AND T2.NOM_STAT='TI_MKG_SCENARIOS'


-- complétion des taux de desabos par jour
drop table if exists #TMP_TI_STATS_DESABO;

CREATE TABLE #TMP_TI_STATS_DESABO (
DT DATE, 
CAMPAIGNID INT,
MAILID INT,
NB_DESABO INT
)
INSERT INTO #TMP_TI_STATS_DESABO (DT,CAMPAIGNID,MAILID,NB_DESABO)
select 
	cast(T3.sent_dt as date) as DT,
	T1.campaignid, 
	T3.MAILID,
	count(distinct(T1.userid)) as NB_DESABO 
from flags T1 with (nolock)
inner join campaigns T2 with (nolock) ON T2.id=T1.campaignid and T2.category='TI_MKG_SCENARIOS' AND T2.run_dt  >= cast(getdate()-(@DELTADAYS+7) as date) 
inner join actionqueue T3 with (nolock) ON T1.inqueueid=T3.inqueueid and T1.CAMPAIGNID=T3.CAMPAIGNID AND T3.sent_dt  >= cast(getdate()-(@DELTADAYS+7) as date)
WHERE 
T1.DT  >= cast(getdate()-(@DELTADAYS+7) as date)
and exists (select 1 from MAIL_PROBES T4 with (nolock) where T4.mailid=t3.mailid and t1.probeid=T4.probeid AND T4.CATEGORY ='OPTOUT')
group by cast(T3.sent_dt as date),T1.campaignid,T3.MAILID


UPDATE T2
SET 
T2.NB_OPTOUT = T1.NB_DESABO 
from #TMP_TI_STATS_DESABO T1
inner join DATA_TI_STATS_DATASTUDIO T2 ON T1.CAMPAIGNID = T2.CAMPAIGNID AND T2.MAILID=T1.MAILID AND T2.DT = T1.DT AND T2.NOM_STAT='TI_MKG_SCENARIOS'



END
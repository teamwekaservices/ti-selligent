AS
BEGIN
SET NOCOUNT ON;

MERGE DATA_TI_STATS_DATASTUDIO AS T
USING (select cast(sent_dt as date) as DT,ACTIONQUEUE.campaignid,
triggerid,count(*) as NB_CIBLE,sum(case when smtperr=250 then 1 else 0 end) as NB_DELIVRE,sum(case when smtperr=250 then 0 else 1 end) as NB_BOUNCES  
,max(CAMPAIGNTRIGGERFLAGS.START_DT) AS START_DT
from ACTIONQUEUE with (nolock)
INNER JOIN CAMPAIGNTRIGGERFLAGS WITH (nolock) ON CAMPAIGNTRIGGERFLAGS.ID = ACTIONQUEUE.TRIGGERID
where exists (select 1 from campaigns with (nolock) where campaigns.id=ACTIONQUEUE.campaignid and campaigns.category='TI_INFOCOLL')
and sent_dt  >= cast(getdate()-2 as date) AND sent_dt < cast(getdate() as date)
group by cast(sent_dt as date),ACTIONQUEUE.campaignid,triggerid) AS S
ON (T.DT=S.DT AND T.CAMPAIGNID=S.CAMPAIGNID AND T.triggerid=S.triggerid AND T.NOM_STAT='TI_INFOCOLL')
WHEN NOT MATCHED BY TARGET
	THEN 
	INSERT (NOM_STAT
		,[DT]
		,CAMPAIGNID
		,[NB_CIBLE]
		,[NB_DELIVRE]
		,[NB_BOUNCES]
		,triggerid
		,start_dt)
		 VALUES ('TI_INFOCOLL'
			   ,S.[DT]
			   ,S.[CAMPAIGNID]
			   ,S.[NB_CIBLE]
			   ,S.[NB_DELIVRE]
			   ,S.NB_BOUNCES
			   ,S.triggerid
			   ,S.start_dt) 
	WHEN MATCHED
		THEN 
			UPDATE SET T.[NB_CIBLE] = S.[NB_CIBLE]
			   ,T.[NB_DELIVRE]=S.NB_DELIVRE
			   ,T.[NB_BOUNCES]=S.NB_BOUNCES
			   ,T.[start_dt]=S.start_dt;
			   


MERGE DATA_TI_STATS_DATASTUDIO AS T
USING (select cast(dt as date) as DT,flags.campaignid,triggerid, count(*) as NB_CLICS, count(distinct(flags.userid)) as NB_U_CLICS, max(CAMPAIGNTRIGGERFLAGS.START_DT) AS START_DT
from flags with (nolock)
INNER JOIN CAMPAIGNTRIGGERFLAGS WITH (nolock) ON CAMPAIGNTRIGGERFLAGS.ID = FLAGS.TRIGGERID
where exists ( select 1 from campaigns with (nolock) where campaigns.id=flags.campaignid and campaigns.category='TI_INFOCOLL')
and dt  >= cast(getdate()-2 as date) AND dt < cast(getdate() as date)
and probeid>200
group by cast(dt as date),flags.campaignid,triggerid) AS S
ON (T.DT=S.DT AND T.CAMPAIGNID=S.CAMPAIGNID AND T.triggerid=S.triggerid AND T.NOM_STAT='TI_INFOCOLL')
WHEN NOT MATCHED BY TARGET
	THEN 
	INSERT (NOM_STAT
		,[DT]
		,[CAMPAIGNID]
		,[NB_CLICS]
		,[NB_U_CLICS]
		,triggerid
		,start_dt)
		 VALUES ('TI_INFOCOLL'
			   ,S.[DT]
			   ,S.[CAMPAIGNID]
			   ,S.[NB_CLICS]
			   ,S.NB_U_CLICS
			   ,S.triggerid
			   ,S.start_dt)
	WHEN MATCHED
		THEN 
			UPDATE SET T.[NB_CLICS] = S.[NB_CLICS]
			   ,T.[NB_U_CLICS]=S.NB_U_CLICS
			   ,T.[start_dt]=S.start_dt;			   
			   
			   

MERGE DATA_TI_STATS_DATASTUDIO AS T
USING (select cast(dt as date) as DT,flags.campaignid,TRIGGERID, count(*) as NB_OUVERTURES, count(distinct(flags.userid)) as NB_U_OUVERTURES, max(CAMPAIGNTRIGGERFLAGS.START_DT) AS START_DT
from flags with (nolock)
INNER JOIN CAMPAIGNTRIGGERFLAGS WITH (nolock) ON CAMPAIGNTRIGGERFLAGS.ID = FLAGS.TRIGGERID
where exists ( select 1 from campaigns with (nolock) where campaigns.id=flags.campaignid and campaigns.category='TI_INFOCOLL')
and dt  >= cast(getdate()-2 as date) AND dt < cast(getdate() as date)
and probeid=-1
group by cast(dt as date),flags.campaignid,TRIGGERID) AS S
ON (T.DT=S.DT AND T.CAMPAIGNID=S.CAMPAIGNID AND T.triggerid=S.triggerid AND T.NOM_STAT='TI_INFOCOLL')
WHEN NOT MATCHED BY TARGET
	THEN 
	INSERT (NOM_STAT
		,[DT]
		,[CAMPAIGNID]
		,[NB_U_OUVERTURES]
		,[NB_OUVERTURES]
		,triggerid
		,start_dt)
		 VALUES ('TI_INFOCOLL'
			   ,S.[DT]
			   ,S.[CAMPAIGNID]
			   ,S.[NB_U_OUVERTURES]
			   ,S.NB_OUVERTURES
			   ,S.triggerid
			   ,S.start_dt) 
	WHEN MATCHED
		THEN 
			UPDATE SET T.[NB_U_OUVERTURES] = S.[NB_U_OUVERTURES]
			   ,T.[NB_OUVERTURES]=S.NB_OUVERTURES
			   ,T.[start_dt]=S.start_dt;
			   

MERGE INTO DATA_TI_STATS_DATASTUDIO AS T
USING (
SELECT CAMPAIGNID,max(VIEWCOUNT) AS VIEWCOUNT,max(UVIEWCOUNT) AS UVIEWCOUNT,max(TARGETCOUNT) AS TARGETCOUNT,max(DELIVERYCOUNT) AS DELIVERYCOUNT,max(BOUNCECOUNT) AS BOUNCECOUNT,
CAMPAIGNS.NAME,max(CLICKCOUNT) AS CLICKCOUNT,max(UCLICKCOUNT) AS UCLICKCOUNT FROM sim_reporting_flowmetrics WITH (NOLOCK)
INNER JOIN CAMPAIGNS WITH (NOLOCK) ON sim_reporting_flowmetrics.CAMPAIGNID=CAMPAIGNS.ID AND CAMPAIGNS.CATEGORY='TI_INFOCOLL'
group by campaignid, name) AS S
ON (T.CAMPAIGNID=S.CAMPAIGNID AND T.NOM_STAT='TI_INFOCOLL_CUMUL')
WHEN NOT MATCHED BY TARGET
	THEN 
	INSERT (DT,CAMPAIGNID,NB_OUVERTURES,NOM_STAT,NB_U_OUVERTURES,NB_CIBLE,NB_DELIVRE,NB_BOUNCES,NOM_CAMPAGNE,NB_CLICS,NB_U_CLICS)
		 VALUES (getdate(),S.CAMPAIGNID,S.VIEWCOUNT,'TI_INFOCOLL_CUMUL',S.UVIEWCOUNT,S.TARGETCOUNT,S.DELIVERYCOUNT,S.BOUNCECOUNT,S.NAME,S.CLICKCOUNT,S.UCLICKCOUNT)
	WHEN MATCHED
		THEN 
			UPDATE SET 
				T.DT=getdate(),
				T.NB_OUVERTURES=S.VIEWCOUNT,
				T.NB_U_OUVERTURES=S.UVIEWCOUNT,
				T.NB_CIBLE=S.TARGETCOUNT,
				T.NB_DELIVRE=S.DELIVERYCOUNT,
				T.NB_BOUNCES=S.BOUNCECOUNT,
				T.NOM_CAMPAGNE=S.NAME,
				T.NB_CLICS=S.CLICKCOUNT,
				T.NB_U_CLICS=S.UCLICKCOUNT;		


-- taux désabo 

MERGE INTO DATA_TI_STATS_DATASTUDIO AS T
USING (
    select cast(dt as date) as DT,t1.campaignid,count(*) AS NB_CLICS,count(distinct(userid)) AS NB_U_CLICS  
    FROM flags t1
    with (nolock)
    inner join CAMPAIGN_ACTIONS t2 with (nolock) on t1.campaignid=t2.campaignid and t1.actionid=t2.actionid and t1.probeid>0
    where exists (select 1 
		  from campaigns with (nolock) 
		  where campaigns.category = 'TI_INFOCOLL' AND t1.campaignid=campaigns.id and campaigns.run_dt>=cast(getdate()-2 as date))
    and exists (select 1 from MAIL_PROBES t3 with (nolock) where t2.mailid=t3.mailid and t1.probeid=t3.probeid AND t3.CATEGORY ='OPTOUT')
    group by cast(dt as date),t1.campaignid
) AS S
ON (T.CAMPAIGNID=S.CAMPAIGNID AND T.NOM_STAT='TI_INFOCOLL' AND T.DT=S.DT) 
WHEN NOT MATCHED BY TARGET
    THEN 
    INSERT (DT,campaignid,nb_optout,nb_u_optout,nom_stat,update_dt)
         VALUES (S.DT,S.CAMPAIGNID,S.nb_clics,S.nb_u_clics,'TI_INFOCOLL',getdate())
    WHEN MATCHED
        THEN 
            UPDATE SET 
        t.nb_optout=S.nb_clics,
        t.nb_u_optout=S.nb_u_clics,
        t.update_dt=getdate()	;	   
			   
UPDATE DATA_TI_STATS_DATASTUDIO 
SET NOM_CAMPAGNE=(select name from campaigns with (nolock) where campaigns.id=DATA_TI_STATS_DATASTUDIO.campaignid)
WHERE NOM_CAMPAGNE IS NULL;
	   			   

UPDATE DATA_TI_STATS_DATASTUDIO 
SET START_DT=(select RUN_DT from campaigns with (nolock) where campaigns.id=DATA_TI_STATS_DATASTUDIO.campaignid)
WHERE START_DT IS NULL AND NOM_STAT='TI_INFOCOLL';

END
@DELTADAYS INT
AS
BEGIN
SET NOCOUNT ON;
MERGE INTO DATA_TI_STATS_DATASTUDIO AS T
USING (
SELECT CAMPAIGNID,max(VIEWCOUNT) AS VIEWCOUNT,max(UVIEWCOUNT) AS UVIEWCOUNT,max(TARGETCOUNT) AS TARGETCOUNT,max(DELIVERYCOUNT) AS DELIVERYCOUNT,max(BOUNCECOUNT) AS BOUNCECOUNT,
CAMPAIGNS.NAME,max(CLICKCOUNT) AS CLICKCOUNT,max(UCLICKCOUNT) AS UCLICKCOUNT FROM sim_reporting_flowmetrics WITH (NOLOCK)
INNER JOIN CAMPAIGNS WITH (NOLOCK) ON sim_reporting_flowmetrics.CAMPAIGNID=CAMPAIGNS.ID AND CAMPAIGNS.CATEGORY='TI_MKG_SCENARIOS'
group by campaignid, name) AS S
ON (T.CAMPAIGNID=S.CAMPAIGNID AND T.NOM_STAT='TI_MKG_SCENARIOS_CUMUL')
WHEN NOT MATCHED BY TARGET
	THEN 
	INSERT (DT,CAMPAIGNID,NB_OUVERTURES,NOM_STAT,NB_U_OUVERTURES,NB_CIBLE,NB_DELIVRE,NB_BOUNCES,NOM_CAMPAGNE,NB_CLICS,NB_U_CLICS)
		 VALUES (getdate(),S.CAMPAIGNID,S.VIEWCOUNT,'TI_MKG_SCENARIOS_CUMUL',S.UVIEWCOUNT,S.TARGETCOUNT,S.DELIVERYCOUNT,S.BOUNCECOUNT,
S.NAME,S.CLICKCOUNT,S.UCLICKCOUNT)
	WHEN MATCHED
		THEN 
			UPDATE SET 
				T.DT=getdate(),
				T.NB_OUVERTURES=S.VIEWCOUNT,
				T.NB_U_OUVERTURES=S.UVIEWCOUNT,
				T.NB_CIBLE=S.TARGETCOUNT,
				T.NB_DELIVRE=S.DELIVERYCOUNT,
				T.NB_BOUNCES=S.BOUNCECOUNT,
				T.NOM_CAMPAGNE=S.NAME,
				T.NB_CLICS=S.CLICKCOUNT,
				T.NB_U_CLICS=S.UCLICKCOUNT;			   
			   

--
-- nb clics / clics uniques 
MERGE DATA_TI_STATS_DATASTUDIO AS T
USING (
	select cast(dt as date) as DT,T1.campaignid, count(*) as NB_CLICS, count(distinct(T1.userid)) as NB_U_CLICS, TA.ACTIONCODE
	from flags T1 with (nolock)
	inner join ACTION_TI_KARIBOU_SCENARIOS TA with (nolock) ON TA.ID = T1.DITEMID
	where exists (select 1 from campaigns with (nolock) where campaigns.id=T1.campaignid and campaigns.category='TI_MKG_SCENARIOS')
	and dt  >= cast(getdate()-@DELTADAYS  as date) AND dt < cast(getdate() as date)
	and probeid>200
	group by cast(dt as date),T1.campaignid,TA.ACTIONCODE
) AS S
ON (T.DT=S.DT AND T.CAMPAIGNID=S.CAMPAIGNID AND T.NOM_STAT='TI_MKG_SCENARIOS' AND T.ACTIONCODE = S.ACTIONCODE)
WHEN NOT MATCHED BY TARGET
	THEN 
	INSERT (NOM_STAT
		,[DT]
		,[CAMPAIGNID]
		,[NB_CLICS]
		,[NB_U_CLICS]
			, ACTIONCODE)
		 VALUES ('TI_MKG_SCENARIOS'
			   ,S.[DT]
			   ,S.[CAMPAIGNID]
			   ,S.[NB_CLICS]
			   ,S.NB_U_CLICS
			   	,S.ACTIONCODE) 
	WHEN MATCHED
		THEN 
			UPDATE SET T.[NB_CLICS] = S.[NB_CLICS]
			   ,T.[NB_U_CLICS]=S.NB_U_CLICS;
			   
			   
			  
			  
 --- 




MERGE DATA_TI_STATS_DATASTUDIO AS T
USING (select cast(dt as date) as DT,T1.campaignid, count(*) as NB_OUVERTURES, count(distinct(T1.userid)) as NB_U_OUVERTURES , TA.ACTIONCODE
from flags T1 with (nolock)
inner join ACTION_TI_KARIBOU_SCENARIOS TA with (nolock) ON TA.ID = T1.DITEMID
where exists (select 1 from campaigns with (nolock) where campaigns.id=T1.campaignid and campaigns.category='TI_MKG_SCENARIOS')
and dt  >= cast(getdate()-@DELTADAYS  as date) AND dt < cast(getdate() as date)
and probeid=-1
group by cast(dt as date),T1.campaignid, TA.ACTIONCODE) AS S
ON (T.DT=S.DT AND T.CAMPAIGNID=S.CAMPAIGNID AND T.NOM_STAT='TI_MKG_SCENARIOS'  AND T.ACTIONCODE = S.ACTIONCODE)
WHEN NOT MATCHED BY TARGET
	THEN 
	INSERT (NOM_STAT
		,[DT]
		,[CAMPAIGNID]
		,[NB_U_OUVERTURES]
		,[NB_OUVERTURES]
			,ACTIONCODE)
		 VALUES ('TI_MKG_SCENARIOS'
			   ,S.[DT]
			   ,S.[CAMPAIGNID]
			   ,S.[NB_U_OUVERTURES]
			   ,S.NB_OUVERTURES
			   	,S.ACTIONCODE) 
	WHEN MATCHED
		THEN 
			UPDATE SET T.[NB_U_OUVERTURES] = S.[NB_U_OUVERTURES]
			   ,T.[NB_OUVERTURES]=S.NB_OUVERTURES;
			   
			   
---
-- Nb de cibles/bounces/livraison
MERGE DATA_TI_STATS_DATASTUDIO AS T
USING (select 
	cast(sent_dt as date) as DT,
	ACTIONQUEUE.campaignid,
	count(*) as NB_CIBLE,
	sum(case when smtperr=250 then 1 else 0 end) as NB_DELIVRE,
	sum(case when smtperr=250 then 0 else 1 end) as NB_BOUNCES,
	TA.ACTIONCODE
from ACTIONQUEUE with (nolock)
inner join ACTION_TI_KARIBOU_SCENARIOS TA with (nolock) ON TA.ID = ACTIONQUEUE.DITEMID
where exists (select 1 from campaigns with (nolock) where campaigns.id=ACTIONQUEUE.campaignid and campaigns.category='TI_MKG_SCENARIOS')
and sent_dt  >= cast(getdate()-@DELTADAYS  as date) AND sent_dt < cast(getdate() as date)
group by cast(sent_dt as date),ACTIONQUEUE.campaignid, TA.ACTIONCODE) AS S
ON (T.DT=S.DT AND T.CAMPAIGNID=S.CAMPAIGNID AND T.NOM_STAT='TI_MKG_SCENARIOS' AND T.ACTIONCODE = S.ACTIONCODE)
WHEN NOT MATCHED BY TARGET
	THEN 
	INSERT (NOM_STAT
		,[DT]
		,CAMPAIGNID
		,[NB_CIBLE]
		,[NB_DELIVRE]
		,[NB_BOUNCES]
			,ACTIONCODE)
		 VALUES ('TI_MKG_SCENARIOS'
			   ,S.[DT]
			   ,S.[CAMPAIGNID]
			   ,S.[NB_CIBLE]
			   ,S.[NB_DELIVRE]
			   ,S.NB_BOUNCES
			   	,S.ACTIONCODE) 
	WHEN MATCHED
		THEN 
			UPDATE SET T.[NB_CIBLE] = S.[NB_CIBLE]
			   ,T.[NB_DELIVRE]=S.NB_DELIVRE
			   ,T.[NB_BOUNCES]=S.NB_BOUNCES;
UPDATE DATA_TI_STATS_DATASTUDIO 
SET NOM_CAMPAGNE=(select name from campaigns with (nolock) where campaigns.id=DATA_TI_STATS_DATASTUDIO.campaignid)
WHERE NOM_CAMPAGNE IS NULL;

UPDATE DATA_TI_STATS_DATASTUDIO 
SET START_DT=(select RUN_DT from campaigns with (nolock) where campaigns.id=DATA_TI_STATS_DATASTUDIO.campaignid)
WHERE START_DT IS NULL AND NOM_STAT='TI_MKG_SCENARIOS';

			   
			  
END
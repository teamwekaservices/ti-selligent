AS
BEGIN
 /* ******************* Documentation Template (max 200 chars per line) ******************** 
  -- DID - Author: TB 
  -- DID - CreationDate: 13/01/2023
  -- DID - Version: 0.1.0 
  -- DID - Description: est executée par Talend via API : Flux 173
  -- DID - Exceptions: Exceptions 
  -- DID - BusinessRules: Rule 
  -- DID - LastModifiedBy: LastModifiedBy 
 ******************* Documentation Template (max 200 chars per line) ********************  */ 
SET NOCOUNT ON;

----------------------------------------------------------------------------------------------------------------------------------------------------------------------
----------------------------------------------------------------------------------------------------------------------------------------------------------------------

           IF OBJECT_ID('dbo.TI_PROFIL_TAXO', 'U') IS NULL
            CREATE TABLE TI_PROFIL_TAXO (
            PROFILE_ID NVARCHAR(100), 
            MAJ_PROFILE_DT DATETIME, 
            PREVIOUS_HASH NVARCHAR(32), 
            CURRENT_HASH NVARCHAR(32), 
            CONSTRAINT IDX_TI_PROFIL_TAXO UNIQUE(PROFILE_ID)
            );

----------------------------------------------------------------------------------------------------------------------------------------------------------------------
----------------------------------------------------------------------------------------------------------------------------------------------------------------------

        UPDATE TI_PROFIL_TAXO
        SET PREVIOUS_HASH = CURRENT_HASH;

----------------------------------------------------------------------------------------------------------------------------------------------------------------------
----------------------------------------------------------------------------------------------------------------------------------------------------------------------

        INSERT INTO TI_PROFIL_TAXO(PROFILE_ID)
        SELECT T1.PROFILE_ID
        FROM USERS_TI_EMAILS T1 WITH (nolock)
        WHERE T1.PROFILE_ID is not null AND T1.PROFILE_ID <> '' AND NOT EXISTS (SELECT 1 FROM TI_PROFIL_TAXO T2 WITH (nolock) WHERE T1.PROFILE_ID = T2.PROFILE_ID);

----------------------------------------------------------------------------------------------------------------------------------------------------------------------
-- Crétion d'un Hash sur l'ensemble des données afin de verifier quelle ligne à été modifier 
----------------------------------------------------------------------------------------------------------------------------------------------------------------------

        UPDATE TI_PROFIL_TAXO 
        SET CURRENT_HASH = CONVERT(NVARCHAR(32),HASHBYTES('MD5', 
        CONCAT( 
              T1.ID
            , T1.OPTOUT
            , T2.MAILS_SENT
            , T2.MAILS_DELIVERED
            , T2.MAILS_VIEWED
            , T2.MAILS_CLICKED
            , T2.DELIVERY_RATE
            , T2.VIEW_RATE
            , T2.CLICK_RATE
            , T2.DELIVERY_LASTDT
            , T2.VIEW_LASTDT
            , T2.CLICK_LASTDT
            , T1.OPTOUT_DT
            , T1.BOUNCEDT
            )),2)
	    FROM USERS_TI_EMAILS T1
        LEFT JOIN TAXONOMY_L156_T4 T2 on T1.ID = T2.USERID 
        WHERE (T1.PROFILE_ID is not null OR T1.PROFILE_ID <> '')  AND T1.PROFILE_ID = TI_PROFIL_TAXO.PROFILE_ID ;

----------------------------------------------------------------------------------------------------------------------------------------------------------------------
----------------------------------------------------------------------------------------------------------------------------------------------------------------------

        UPDATE TI_PROFIL_TAXO
        SET MAJ_PROFILE_DT = cast(getdate() as date)
        WHERE CURRENT_HASH <> PREVIOUS_HASH  OR PREVIOUS_HASH is null;

----------------------------------------------------------------------------------------------------------------------------------------------------------------------
----------------------------------------------------------------------------------------------------------------------------------------------------------------------

        SELECT 
            T1.PROFILE_ID,
            T1.ID as SELLIGENT_USER_ID,
            T1.OPTOUT as SELL_OPTOUT,
            T2.MAILS_SENT as SELLIGENT_MAIL_SENT,
            T2.MAILS_DELIVERED as SELLIGENT_MAIL_DELIVERED,
            T2.MAILS_VIEWED as SELLIGENT_MAIL_VIEWED,
            T2.MAILS_CLICKED as  SELLIGENT_MAIL_CLICKED,
            ROUND(T2.DELIVERY_RATE, 3) as SELLIGENT_DELIVERY_RATE,
            ROUND(T2.VIEW_RATE, 3) as SELLIGENT_VIEW_RATE,
            ROUND(T2.CLICK_RATE,3) as SELLIGENT_CLICK_RATE,
            FORMAT(T2.DELIVERY_LASTDT ,'yyyy-MM-dd HH:mm:ss') as SELL_DELIVERY_LAST_DT,
            FORMAT(T2.VIEW_LASTDT,'yyyy-MM-dd HH:mm:ss') as SELL_VIEW_LAST_DT,
            FORMAT(T2.CLICK_LASTDT,'yyyy-MM-dd HH:mm:ss') as SELL_CLICK_LAST_DT,
            FORMAT(T1.OPTOUT_DT,'yyyy-MM-dd HH:mm:ss') as SELL_OPTOUT_DT,
            FORMAT(T1.BOUNCEDT,'yyyy-MM-dd HH:mm:ss') as SELL_BOUNCE_DT
        FROM USERS_TI_EMAILS T1 with (nolock)
        LEFT JOIN TAXONOMY_L156_T4 T2 with (nolock) on T1.ID = T2.USERID
        WHERE T1.profile_id is not null
        AND EXISTS (SELECT 1 FROM TI_PROFIL_TAXO T2 WHERE (PREVIOUS_HASH <> CURRENT_HASH OR PREVIOUS_HASH is null) AND T1.PROFILE_ID = T2.PROFILE_ID) ;
                
END
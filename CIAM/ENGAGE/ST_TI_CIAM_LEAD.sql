AS
BEGIN
SET NOCOUNT ON;
MERGE DBO.DATA_TI_CIAM_LEAD AS T
USING(
	SELECT 
lead_id,
creation_date,
source_form,
appointment_request,
utm_campaign,
product_sku,
product_name,
thematic,
utm_source,
utm_medium,
utm_content,
referrer_url,
actor,
result_status,
creation_date_crm,
processing_date_crm,
gotowebinar_key,
id_leadsfactory,
f_profile_id,
b_upddate,
treatment_status,
univers,
linked_content
	FROM Tmp_Ti_Ciam_Lead) AS S
ON (T.lead_id = S.lead_id)
WHEN NOT MATCHED BY TARGET
	THEN
		INSERT
	(lead_id,
creation_date,
source_form,
appointment_request,
utm_campaign,
product_sku,
product_name,
thematic,
utm_source,
utm_medium,
utm_content,
referrer_url,
actor,
result_status,
creation_date_crm,
processing_date_crm,
gotowebinar_key,
id_leadsfactory,
f_profile_id,
b_upddate,
treatment_status,
univers,
linked_content
)
        VALUES(
            S.lead_id,
S.creation_date,
S.source_form,
S.appointment_request,
S.utm_campaign,
S.product_sku,
S.product_name,
S.thematic,
S.utm_source,
S.utm_medium,
S.utm_content,
S.referrer_url,
S.actor,
S.result_status,
S.creation_date_crm,
S.processing_date_crm,
S.gotowebinar_key,
S.id_leadsfactory,
S.f_profile_id,
S.b_upddate,
S.treatment_status,
S.univers,
S.linked_content
)
WHEN MATCHED
	THEN
		UPDATE SET
	
T.creation_date = S.creation_date,
T.source_form = S.source_form,
T.appointment_request = S.appointment_request,
T.utm_campaign = S.utm_campaign,
T.product_sku = S.product_sku,
T.product_name = S.product_name,
T.thematic = S.thematic,
T.utm_source = S.utm_source,
T.utm_medium = S.utm_medium,
T.utm_content = S.utm_content,
T.referrer_url = S.referrer_url,
T.actor = S.actor,
T.result_status = S.result_status,
T.creation_date_crm = S.creation_date_crm,
T.processing_date_crm = S.processing_date_crm,
T.gotowebinar_key = S.gotowebinar_key,
T.id_leadsfactory = S.id_leadsfactory,
T.f_profile_id = S.f_profile_id,
T.b_upddate = S.b_upddate,
T.treatment_status = S.treatment_status,
T.univers = S.univers,
T.linked_content = S.linked_content
;
END
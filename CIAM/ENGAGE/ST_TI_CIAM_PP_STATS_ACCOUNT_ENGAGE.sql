AS
BEGIN
SET NOCOUNT ON;
IF (select count(*) from TMP_TI_CIAM_PP_STATS_ACCOUNT) > 0
BEGIN
	truncate table DATA_TI_CIAM_PP_STATS_ACCOUNT;
	insert into DATA_TI_CIAM_PP_STATS_ACCOUNT
	(
		CREATED_DT,
        ACCOUNT_ID,
        NB_ESSAI_GRATUIT,
        NB_ENTREE_DEMO,
        NB_DLD_FICHE,
        NB_QUIZ_REALISATION,
        NB_DLD_OUTIL,
        NB_ENTREE
	)
	select GETDATE(), 
            F_ACCOUNT_ID,
            NB_ESSAI_GRATUIT,
            NB_ENTREE_DEMO,
            NB_DLD_FICHE,
            NB_QUIZ_REALISATION,
            NB_DLD_OUTIL,
            NB_ENTREE from TMP_TI_CIAM_PP_STATS_ACCOUNT
END 

END
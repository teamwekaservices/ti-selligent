AS
BEGIN
SET NOCOUNT ON;

MERGE DATA_TI_ODOO_OPPORTUNITY AS T
	USING (
		SELECT 	T1.ID,
		T1.TI_ODOO_OPPORTUNITY_ID,
		T1.TYPE,
		ACTIVE,
		SOURCE_FORM,
        UTM_CAMPAIGN,
        UTM_SOURCE,
        UTM_MEDIUM,
        UTM_CONTENT,
        PRODUCTS,
        ESTIMATE_CLOSING,
        LOST_REASON,
        ESTIMATE_INCOME,
        END_CONTRACT,
		SCORING,
        CREATION_DATE,
        CLOSING_DATE,
        PIPE,
        STAGE,
        UNIVERS,
        WON_STATUS,
        SALES_TYPE,
        LEAD_ID,
        F_PROFILE_ID,
        BIZ_DEV_ASSIGNED_TO,
        BIZ_DEV_CREATOR,
        REFERENTIEL_ENTREPRISE
		FROM DBO.TMP_TI_ODOO_OPPORTUNITY T1

		) AS S
	ON (T.TI_ODOO_OPPORTUNITY_ID = S.TI_ODOO_OPPORTUNITY_ID) 
	WHEN NOT MATCHED BY TARGET
		THEN 
		INSERT(
                CREATED_DT,
		        TI_ODOO_OPPORTUNITY_ID,
                TYPE,
                ACTIVE,
                SOURCE_FORM,
                UTM_CAMPAIGN,
                UTM_SOURCE,
                UTM_MEDIUM,
                UTM_CONTENT,
                PRODUCTS,
                ESTIMATE_CLOSING,
                LOST_REASON,
                ESTIMATE_INCOME,
                END_CONTRACT,
				SCORING,
                CREATION_DATE,
                CLOSING_DATE,
                PIPE,
                STAGE,
                UNIVERS,
                WON_STATUS,
                SALES_TYPE,
                LEAD_ID,
                F_PROFILE_ID,
                BIZ_DEV_ASSIGNED_TO,
                BIZ_DEV_CREATOR,
                REFERENTIEL_ENTREPRISE
		)
		VALUES(
            GETDATE(),
            S.TI_ODOO_OPPORTUNITY_ID,
            S.TYPE,
            S.ACTIVE,
            S.SOURCE_FORM,
            S.UTM_CAMPAIGN,
            S.UTM_SOURCE,
            S.UTM_MEDIUM,
            S.UTM_CONTENT,
            S.PRODUCTS,
            S.ESTIMATE_CLOSING,
            S.LOST_REASON,
            S.ESTIMATE_INCOME,
            S.END_CONTRACT,
            S.SCORING,
            S.CREATION_DATE,
            S.CLOSING_DATE,
            S.PIPE,
            S.STAGE,
            S.UNIVERS,
            S.WON_STATUS,
            S.SALES_TYPE,
            S.LEAD_ID,
            S.F_PROFILE_ID,
            S.BIZ_DEV_ASSIGNED_TO,
            S.BIZ_DEV_CREATOR,
            S.REFERENTIEL_ENTREPRISE) 
	WHEN MATCHED 
		THEN 
		UPDATE SET T.MODIFIED_DT = GETDATE(),
			T.TI_ODOO_OPPORTUNITY_ID = S.TI_ODOO_OPPORTUNITY_ID,
			T.TYPE = S.TYPE,
			T.ACTIVE = S.ACTIVE,
			T.SOURCE_FORM = S.SOURCE_FORM,
			T.UTM_CAMPAIGN = S.UTM_CAMPAIGN,
			T.UTM_SOURCE = S.UTM_SOURCE,
			T.UTM_MEDIUM = S.UTM_MEDIUM,
			T.UTM_CONTENT = S.UTM_CONTENT,
            T.PRODUCTS = S.PRODUCTS,
			T.ESTIMATE_CLOSING = S.ESTIMATE_CLOSING,
			T.LOST_REASON = S.LOST_REASON,
			T.ESTIMATE_INCOME = S.ESTIMATE_INCOME,
			T.END_CONTRACT = S.END_CONTRACT,
			T.SCORING = S.SCORING,
            T.CREATION_DATE = S.CREATION_DATE,
            T.CLOSING_DATE = S.CLOSING_DATE,
            T.PIPE = S.PIPE,
            T.STAGE = S.STAGE,
            T.UNIVERS = S.UNIVERS,
            T.WON_STATUS = S.WON_STATUS,
            T.SALES_TYPE = S.SALES_TYPE,
            T.LEAD_ID = S.LEAD_ID,
            T.F_PROFILE_ID = S.F_PROFILE_ID,
            T.BIZ_DEV_ASSIGNED_TO = S.BIZ_DEV_ASSIGNED_TO,
            T.BIZ_DEV_CREATOR = S.BIZ_DEV_CREATOR,
            T.REFERENTIEL_ENTREPRISE = S.REFERENTIEL_ENTREPRISE;

END
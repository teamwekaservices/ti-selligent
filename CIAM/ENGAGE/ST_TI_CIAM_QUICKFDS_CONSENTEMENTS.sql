AS
BEGIN
	-- SET NOCOUNT ON ADDED TO PREVENT EXTRA RESULT SETS FROM
	-- INTERFERING WITH SELECT STATEMENTS.
	SET NOCOUNT ON;

	MERGE DBO.DATA_TI_QUICKFDS_CONSENTEMENTS AS T
	USING (SELECT 
			ti_quick_fds_consentement,
			USERS_TI_EMAILS.ID AS MAIL_CODE,
			GETDATE() AS UPDATE_DT,
			quick_mkg_email,
			quick_part_email,
			quick_edito_email,
			f_profile_id
		FROM Tmp_TI_QUICKFDS_CONSENTEMENTS
		LEFT JOIN USERS_TI_EMAILS ON USERS_TI_EMAILS.MAIL = Tmp_TI_QUICKFDS_CONSENTEMENTS.ti_quick_fds_consentement 
		WHERE OPTI_REJECTED = 0
	) AS S 
	ON (T.ti_quick_fds_consentement = S.ti_quick_fds_consentement) 
	WHEN NOT MATCHED BY TARGET
		THEN 
			INSERT(
				ti_quick_fds_consentement,
				MAIL_CODE,
				UPDATE_DT,
				quick_mkg_email,
				quick_part_email,
				quick_edito_email,
				f_profile_id
			)
			VALUES( 
				S.ti_quick_fds_consentement,
				S.MAIL_CODE,
				S.UPDATE_DT,
				S.quick_mkg_email,
				S.quick_part_email,
				S.quick_edito_email,
				S.f_profile_id
			) 
	WHEN MATCHED
		THEN 
			UPDATE SET T.MAIL_CODE=S.MAIL_CODE,
			T.UPDATE_DT=S.UPDATE_DT,
				T.quick_mkg_email=S.quick_mkg_email,
				T.quick_part_email=S.quick_part_email,
				T.quick_edito_email=S.quick_edito_email,
				T.f_profile_id=S.f_profile_id;
			
			
			
END
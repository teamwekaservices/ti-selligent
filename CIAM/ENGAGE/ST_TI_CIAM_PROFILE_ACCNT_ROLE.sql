AS
BEGIN
SET NOCOUNT ON;

MERGE DBO.DATA_TI_CIAM_PROFILE_ACCNT_ROL AS T
USING(
	SELECT 
	profile_accnt_role_id,
	role,
	subscriber_type,
	subscription_type,
	f_profile_id,
	f_account_id,
	b_upddate
	FROM Tmp_Ti_Ciam_Profile_Accnt_Rol) AS S
ON (T.profile_accnt_role_id = S.profile_accnt_role_id)
WHEN NOT MATCHED BY TARGET
	THEN
		INSERT
	(profile_accnt_role_id,
	role_type,
	subscriber_type,
	subscription_type,
	f_profile_id,
	f_account_id,
    b_upddate)
        VALUES(S.profile_accnt_role_id,
	S.role,
	S.subscriber_type,
	S.subscription_type,
	S.f_profile_id,
	S.f_account_id,
    S.b_upddate)
WHEN MATCHED
	THEN
		UPDATE SET
	T.role_type = S.role,
T.subscriber_type = S.subscriber_type,
T.subscription_type = S.subscription_type,
T.f_profile_id = S.f_profile_id,
T.f_account_id = S.f_account_id,
T.b_upddate = S.b_upddate;
END
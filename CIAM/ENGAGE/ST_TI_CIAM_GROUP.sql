AS
BEGIN
SET NOCOUNT ON;

MERGE DBO.DATA_TI_CIAM_GROUP AS T
USING(
	SELECT 
	ti_group_id,
	nom_groupe
	FROM Tmp_Ti_Ciam_Group) AS S
ON (T.ti_group_id = S.ti_group_id)
WHEN NOT MATCHED BY TARGET
	THEN
		INSERT
	(ti_group_id,
	nom_groupe)
        VALUES(S.ti_group_id,
	S.nom_groupe)
WHEN MATCHED
	THEN
		UPDATE SET
	
	T.nom_groupe = S.nom_groupe;
END
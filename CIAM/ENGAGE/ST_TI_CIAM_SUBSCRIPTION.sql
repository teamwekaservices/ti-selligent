AS
BEGIN
SET NOCOUNT ON;
-- flux 159
-- c'est un flux full on vide donc la table et on recharge

TRUNCATE TABLE DATA_TI_CIAM_SUBSCRIPTIONS

INSERT INTO DATA_TI_CIAM_SUBSCRIPTIONS (account_subscription_id 
,order_id 
,licence_count 
,valid_from 
,valid_to 
,cancelled_at 
,offer_template 
,active_from 
,active_to 
,quantity 
,status 
,subscription_type 
,f_ti_ciam_account 
,f_ti_ciam_product 
, b_upddate )
select 
account_subscription_id 
,order_id 
,licence_count 
,valid_from 
,valid_to 
,cancelled_at 
,offer_template 
,active_from 
,active_to 
,quantity 
,status 
,subscription_type 
,f_ti_ciam_account 
,f_ti_ciam_product 
, b_upddate 
from Tmp_Ti_CIam_Subscription
where SIM_PARSELINE_RESULT=1 and OPTI_REJECTED=0 
and not exists (select 1 from DATA_TI_CIAM_SUBSCRIPTIONS where DATA_TI_CIAM_SUBSCRIPTIONS.account_subscription_id = Tmp_Ti_CIam_Subscription.account_subscription_id)


END
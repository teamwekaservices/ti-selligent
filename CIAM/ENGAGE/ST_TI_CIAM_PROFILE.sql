AS
BEGIN
SET NOCOUNT ON;

IF NOT EXISTS (SELECT name FROM sysindexes WHERE name = 'Idx_Tmp_Ti_Ciam_Profile_MAIL')
create index Idx_Tmp_Ti_Ciam_Profile_MAIL
on Tmp_Ti_Ciam_Profile(EMAIL);

IF NOT EXISTS (SELECT name FROM sysindexes WHERE name = 'Idx_Tmp_Ti_Ciam_Profile_PROFILEID')
create index Idx_Tmp_Ti_Ciam_Profile_PROFILEID
on Tmp_Ti_Ciam_Profile(PROFILE_ID);

-- mise en optout d'une ancienne adresse email pour un profil qui a une adresse dont la nouvelle existe déjà dans Selligent
UPDATE A 
SET A.OPTOUT = 60
,A.OPTOUT_DT = GETDATE()
,A.OPTOUT_SOURCE = 'changement email flux 159'
,A.PROFILE_ID = null 
FROM USERS_TI_EMAILS A WITH (NOlOCK)
WHERE EXISTS (SELECT 1 FROM Tmp_Ti_Ciam_Profile B WHERE A.PROFILE_ID = B.PROFILE_ID AND A.MAIL <> B.EMAIL
AND EXISTS (SELECT 1 FROM USERS_TI_EMAILS C WITH (NOlOCK) WHERE B.EMAIL = C.MAIL )
) 




--MAJ du champ "ID_PROFILE" si il n'est pas renseigné dans la table "USERS_TI_EMAILS"
UPDATE A 
SET A.PROFILE_ID = B.PROFILE_ID 
FROM USERS_TI_EMAILS A WITH (NOlOCK)
INNER JOIN Tmp_Ti_Ciam_Profile B on A.MAIL = B.EMAIL
WHERE A.PROFILE_ID is null;

--MAJ du champ "MAIL" de la table "USERS_TI_EMAILS" si il est different de celui de la table "Tmp_Ti_Ciam_Profile"
UPDATE A 
SET A.MAIL = B.EMAIL
,A.OPTOUT = NULL
,A.OPTOUT_DT = NULL 
,A.OPTOUT_SOURCE = NULL
FROM USERS_TI_EMAILS A WITH (NOlOCK)
INNER JOIN Tmp_Ti_Ciam_Profile B on A.PROFILE_ID = B.PROFILE_ID AND A.MAIL <> B.EMAIL AND ( OPTOUT <> 60 OR OPTOUT IS NULL);

-- création des adresses emails qui n'existent pas 
INSERT INTO USERS_TI_EMAILS (CREATED_DT, MAIL, PROFILE_ID, SUBSCRIBE_SOURCE, UNIVERSTI, UNIVERSKREACCTP,UNIVERSQUICK,F_TI_BIZ_DEV,F_TI_CIAM_PROFILE_ACCNT_ROL,F_ACCOUNT_ID) 
select  CREATION_DATE,EMAIL,PROFILE_ID,SYSTEME_SOURCE,UNIVERSTI,UNIVERSKREACCTP,UNIVERSQUICK,F_TI_BIZ_DEV,F_TI_CIAM_PROFILE_ACCNT_ROL,F_ACCOUNT_ID  from Tmp_Ti_Ciam_Profile T1
where not exists (select 1 from USERS_TI_EMAILS T2 with (nolock) where T1.EMAIL = T2.MAIL);

UPDATE  USERS_TI_EMAILS 
SET
UNIVERSTI =Tmp_Ti_Ciam_Profile.UNIVERSTI , 
UNIVERSKREACCTP=Tmp_Ti_Ciam_Profile.UNIVERSKREACCTP, 
UNIVERSQUICK=Tmp_Ti_Ciam_Profile.UNIVERSQUICK,
F_TI_BIZ_DEV=Tmp_Ti_Ciam_Profile.F_TI_BIZ_DEV,
F_TI_CIAM_PROFILE_ACCNT_ROL=Tmp_Ti_Ciam_Profile.F_TI_CIAM_PROFILE_ACCNT_ROL,
F_ACCOUNT_ID=Tmp_Ti_Ciam_Profile.F_ACCOUNT_ID
FROM Tmp_Ti_Ciam_Profile 
where USERS_TI_EMAILS.PROFILE_ID = Tmp_Ti_Ciam_Profile.PROFILE_ID


-- update des mail_code 
UPDATE USERS_TI_EMAILS 
SET MAIL_CODE = ID 
WHERE MAIL_CODE IS NULL;

MERGE DBO.DATA_TI_CIAM_PROFILE AS T
USING(
	SELECT 
workforce,
user_id,
universti,
universkreacctp,
systeme_source,
subscription_type,
subscriber_type,
subscribe_source,
siret,
silhouette,
role,
profile_type,
profile_id,
postal_code,
phone,
login,
last_password_change,
last_name,
last_modification,
last_download_date,
last_authentication,
f_ti_group,
f_service,
f_referentiel_entreprise,
f_ip_resolved_country,
f_fonction,
f_country,
enabled,
email_to_check,
email,
domain_name,
creation_date,
company_name_perso,
civilite,
city,
address,
univers_pub,
universquick,
f_ti_biz_dev,
f_ti_ciam_profile_accnt_rol,
f_account_id
	FROM Tmp_Ti_Ciam_Profile) AS S
ON (T.profile_id = S.profile_id)
WHEN NOT MATCHED BY TARGET
	THEN
		INSERT
	(workforce,
user_id,
universti,
universkreacctp,
systeme_source,
subscription_type,
subscriber_type,
subscribe_source,
siret,
silhouette,
role_type,
profile_type,
profile_id,
postal_code,
phone,
login,
last_password_change,
last_name,
last_modification,
last_download_date,
last_authentication,
f_ti_group,
f_service,
f_referentiel_entreprise,
f_ip_resolved_country,
f_fonction,
f_country,
enabled,
email_to_check,
email,
domain_name,
creation_date,
company_name_perso,
civilite,
city,
address,
univers_pub,
universquick,
f_ti_biz_dev,
f_ti_ciam_profile_accnt_rol,
f_account_id
)
        VALUES(
            S.workforce,
S.user_id,
S.universti,
S.universkreacctp,
S.systeme_source,
S.subscription_type,
S.subscriber_type,
S.subscribe_source,
S.siret,
S.silhouette,
S.role,
S.profile_type,
S.profile_id,
S.postal_code,
S.phone,
S.login,
S.last_password_change,
S.last_name,
S.last_modification,
S.last_download_date,
S.last_authentication,
S.f_ti_group,
S.f_service,
S.f_referentiel_entreprise,
S.f_ip_resolved_country,
S.f_fonction,
S.f_country,
S.enabled,
S.email_to_check,
S.email,
S.domain_name,
S.creation_date,
S.company_name_perso,
S.civilite,
S.city,
S.address,
S.univers_pub,
S.universquick,
S.f_ti_biz_dev,
S.f_ti_ciam_profile_accnt_rol,
S.f_account_id
)
WHEN MATCHED
	THEN
		UPDATE SET

 T.workforce = S.workforce,
T.user_id = S.user_id,
T.universti = S.universti,
T.universkreacctp = S.universkreacctp,
T.systeme_source = S.systeme_source,
T.subscription_type = S.subscription_type,
T.subscriber_type = S.subscriber_type,
T.subscribe_source = S.subscribe_source,
T.siret = S.siret,
T.silhouette = S.silhouette,
T.role_type = S.role,
T.profile_type = S.profile_type,
T.postal_code = S.postal_code,
T.phone = S.phone,
T.login = S.login,
T.last_password_change = S.last_password_change,
T.last_name = S.last_name,
T.last_modification = S.last_modification,
T.last_download_date = S.last_download_date,
T.last_authentication = S.last_authentication,
T.f_ti_group = S.f_ti_group,
T.f_service = S.f_service,
T.f_referentiel_entreprise = S.f_referentiel_entreprise,
T.f_ip_resolved_country = S.f_ip_resolved_country,
T.f_fonction = S.f_fonction,
T.f_country = S.f_country,
T.enabled = S.enabled,
T.email_to_check = S.email_to_check,
T.email = S.email,
T.domain_name = S.domain_name,
T.creation_date = S.creation_date,
T.company_name_perso = S.company_name_perso,
T.civilite = S.civilite,
T.city = S.city,
T.address = S.address,
T.univers_pub = S.univers_pub,
T.universquick = S.universquick,
T.f_ti_biz_dev = S.f_ti_biz_dev,
T.f_ti_ciam_profile_accnt_rol = S.f_ti_biz_dev,
T.f_account_id = S.f_ti_biz_dev;
	
-- mise à jour du MAIL_CODE
UPDATE T1
SET T1.MAIL_CODE=T2.ID 
FROM DATA_TI_CIAM_PROFILE T1 
INNER JOIN USERS_TI_EMAILS T2 ON T2.PROFILE_ID=T1.PROFILE_ID
WHERE T1.MAIL_CODE IS NULL

END
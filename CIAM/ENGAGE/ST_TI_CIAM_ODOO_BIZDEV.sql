AS
BEGIN
SET NOCOUNT ON;

MERGE DATA_TI_ODOO_BIZDEV AS T
	USING (
		SELECT 	T1.ID,
		T1.TI_BIZ_DEV_ID,
        IDENTIFIANT_SAP,
        ODOO_IDENTIFIANT,
        PRENOM,
        NOM,
        EMAIL,
        TELEPHONE,
        CODE_TA,
        LOGIN_SAP,
        IDENTIFIANT_SAP_DESABO,
        URL_PHOTO,
        URL_PROMO_SIGNATURE,
        FONCTION,
        URL_PRISE_RDV
		FROM DBO.TMP_TI_ODOO_BIZDEV T1
		) AS S
	ON (T.TI_BIZ_DEV_ID = S.TI_BIZ_DEV_ID) 
	WHEN NOT MATCHED BY TARGET
		THEN 
		INSERT(
                CREATED_DT,
		        TI_BIZ_DEV_ID,
                IDENTIFIANT_SAP,
                ODOO_IDENTIFIANT,
                PRENOM,
                NOM,
                EMAIL,
                TELEPHONE,
                CODE_TA,
                LOGIN_SAP,
                IDENTIFIANT_SAP_DESABO,
                URL_PHOTO,
                URL_PROMO_SIGNATURE,
                FONCTION,
                URL_PRISE_RDV
		)
		VALUES(
            GETDATE(),
            S.TI_BIZ_DEV_ID,
            S.IDENTIFIANT_SAP,
            S.ODOO_IDENTIFIANT,
            S.PRENOM,
            S.NOM,
            S.EMAIL,
            S.TELEPHONE,
            S.CODE_TA,
            S.LOGIN_SAP,
            S.IDENTIFIANT_SAP_DESABO,
            S.URL_PHOTO,
            S.URL_PROMO_SIGNATURE,
            S.FONCTION,
            S.URL_PRISE_RDV) 
	WHEN MATCHED 
		THEN 
		UPDATE SET T.MODIFIED_DT = GETDATE(),
			T.TI_BIZ_DEV_ID = S.TI_BIZ_DEV_ID,
			T.IDENTIFIANT_SAP = S.IDENTIFIANT_SAP,
			T.ODOO_IDENTIFIANT = S.ODOO_IDENTIFIANT,
			T.PRENOM = S.PRENOM,
            T.NOM = S.NOM,
			T.EMAIL = S.EMAIL,
			T.TELEPHONE = S.TELEPHONE,
			T.CODE_TA = S.CODE_TA,
			T.LOGIN_SAP = S.LOGIN_SAP,
			T.IDENTIFIANT_SAP_DESABO = S.IDENTIFIANT_SAP_DESABO,
            T.URL_PHOTO = S.URL_PHOTO,
            T.URL_PROMO_SIGNATURE = S.URL_PROMO_SIGNATURE,
            T.FONCTION = S.FONCTION,
            T.URL_PRISE_RDV = S.URL_PRISE_RDV;

END
AS
BEGIN
SET NOCOUNT ON;
MERGE INTO DATA_TI_FICHIERS_EXTERNES AS T
USING (
select ti_fichiers_externes_id, import_date, file_name, f_profile_id from Tmp_TI_FICHIERS_EXTERNES) AS S
ON (T.TI_FICHIERS_EXTERNES_ID=S.ti_fichiers_externes_id)
WHEN NOT MATCHED BY TARGET
	THEN 
	INSERT (TI_FICHIERS_EXTERNES_ID, import_date, file_name, f_profile_id)
	VALUES (S.ti_fichiers_externes_id,S.import_date,S.file_name,S.f_profile_id)
	WHEN MATCHED
		THEN 
			UPDATE SET 
				T.TI_FICHIERS_EXTERNES_ID=S.ti_fichiers_externes_id,
				T.import_date=S.import_date,
				T.file_name=S.file_name,
				T.f_profile_id=S.f_profile_id;			   

END
AS
BEGIN
SET NOCOUNT ON;
IF (select count(*) from TMP_TI_CIAM_PP_STATS_PROFIL) > 0
BEGIN
	truncate table DATA_TI_CIAM_PP_STATS_PROFIL;
	insert into DATA_TI_CIAM_PP_STATS_PROFIL
	(
		CREATED_DT,
                PROFILE_ID,
                NB_ESSAI_GRATUIT,
                NB_ENTREE_DEMO,
                NB_DLD_FICHE,
                NB_QUIZ_REALISATION,
                NB_DLD_OUTIL,
                NB_ENTREE
	)
	select GETDATE(), F_PROFILE_ID,
                NB_ESSAI_GRATUIT,
                NB_ENTREE_DEMO,
                NB_DLD_FICHE,
                NB_QUIZ_REALISATION,
                NB_DLD_OUTIL,
                NB_ENTREE from TMP_TI_CIAM_PP_STATS_PROFIL
END 

END
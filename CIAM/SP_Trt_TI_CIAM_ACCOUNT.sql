AS
BEGIN
SET NOCOUNT ON;
IF (select count(*) from TMP_TI_CIAM_ACCOUNT) > 0
	truncate table DATA_TI_CIAM_ACCOUNT;
	insert into DATA_TI_CIAM_ACCOUNT
	(
		CREATED_DT, 
        ACCOUNT_ID,
        SYSTEME_SOURCE,
        COMPANY_NAME,
        COMPANY_CUSTOMER_GROUP,
        COMPANY_PRICE_CLASS,
        B_UPDDATE,
        TYPE_CLIENT,
        ABO_EN_COURS,
        NB_CONSULT_PDF,
        NB_CONSULT_HTML,
        NB_SQR,
        NB_ARTICLE_DECOUVERTE,
        NB_CERTI,
        NB_LICENCES_ACTIVES,
        ORGANISME_ID,
        SUBSCRIBER_TYPE,
        SUBSCRIPTION_TYPE,
        PERMISSION_PRINT,
        PERMISSION_DOWNLOAD,
        NB_VIEW_LISEUSE
	)
    select GETDATE(), 
            ACCOUNT_ID,
            SYSTEME_SOURCE,
            COMPANY_NAME,
            COMPANY_CUSTOMER_GROUP,
            COMPANY_PRICE_CLASS,
            B_UPDDATE,
            TYPE_CLIENT,
            ABO_ENCOURS,
            NB_CONSULT_PDF,
            NB_CONSULT_HTML,
            NB_SQR,
            NB_ARTICLE_DECOUVERTE,
            NB_CER_TI,
            ACTIVE_LICENCE_COUNT,
            ORGANISME_ID,
            SUBSCRIBER_TYPE,
            SUBSCRIPTION_TYPE,
            PERMISSION_PRINT,
            PERMISSION_DOWNLOAD,
            NB_VIEW_LISEUSE from TMP_TI_CIAM_ACCOUNT   

END
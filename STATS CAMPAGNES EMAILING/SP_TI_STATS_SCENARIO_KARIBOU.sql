@DELTADAYS INT
AS
BEGIN
SET NOCOUNT ON;
		   
			   

--
-- nb clics / clics uniques 
MERGE DATA_TI_STATS_DATASTUDIO AS T
USING (
	SELECT cast(dt as date) as DT,T1.campaignid, count(*) as NB_CLICS, count(distinct(T1.userid)) as NB_U_CLICS,T4.code_action
	FROM flags T1 with (nolock)
	inner join actionqueue T5 with (nolock) on T1.inqueueid = T5.inqueueid AND T5.dlistid = 434
	INNER JOIN ACTION_TI_KARIBOU_SCENARIOS T2 with (nolock) ON T2.ID = T1.DITEMID
	INNER JOIN ARTICLES_TI_KARIBOU_SCENARIOS_REF T4 with (nolock) ON T4.id = T2.id_scenario
	WHERE EXISTS (SELECT 1 FROM campaigns with (nolock) WHERE campaigns.id=T1.campaignid AND campaigns.category='TI_MKG_SCENARIOS')
	AND dt  >= cast(getdate()-@DELTADAYS as date) AND dt < cast(getdate() as date)
	AND probeid>200
	group by cast(dt as date),T1.campaignid,T4.code_action
) AS S
ON (T.DT=S.DT AND T.CAMPAIGNID=S.CAMPAIGNID AND T.NOM_STAT='SP_TI_STATS_SCENARIO_KARIBOU' AND T.code_action = S.code_action)
WHEN NOT MATCHED BY TARGET
	THEN 
	INSERT (NOM_STAT
		,[DT]
		,[CAMPAIGNID]
		,[NB_CLICS]
		,[NB_U_CLICS]
        
			, code_action)
		 VALUES ('SP_TI_STATS_SCENARIO_KARIBOU'
			   ,S.[DT]
			   ,S.[CAMPAIGNID]
			   ,S.[NB_CLICS]
			   ,S.NB_U_CLICS
               
			   	,S.code_action) 
	WHEN MATCHED
		THEN 
			UPDATE SET T.[NB_CLICS] = S.[NB_CLICS]
			   ,T.[NB_U_CLICS]=S.NB_U_CLICS;
			   
			   
			  
			  
 --- 




MERGE DATA_TI_STATS_DATASTUDIO AS T
USING (SELECT cast(dt as date) as DT,T1.campaignid, count(*) as NB_OUVERTURES, count(distinct(T1.userid)) as NB_U_OUVERTURES, T4.code_action
FROM flags T1 with (nolock)
inner join actionqueue T5 with (nolock) on T1.inqueueid = T5.inqueueid AND T5.dlistid = 434
INNER JOIN ACTION_TI_KARIBOU_DOC_QUIZZ T2 with (nolock) ON T2.ID = T1.DITEMID
INNER JOIN ARTICLES_TI_KARIBOU_SCENARIOS_REF T4 with (nolock) ON T4.id = T2.id_scenario
WHERE EXISTS (SELECT 1 FROM campaigns with (nolock) WHERE campaigns.id=T1.campaignid AND campaigns.category='TI_MKG_SCENARIOS')
AND dt  >= cast(getdate()-@DELTADAYS  as date) AND dt < cast(getdate() as date)
AND probeid=-1
group by cast(dt as date),T1.campaignid, T4.code_action) AS S
ON (T.DT=S.DT AND T.CAMPAIGNID=S.CAMPAIGNID AND T.NOM_STAT='SP_TI_STATS_SCENARIO_KARIBOU'  AND T.code_action = S.code_action)
WHEN NOT MATCHED BY TARGET
	THEN 
	INSERT (NOM_STAT
		,[DT]
		,[CAMPAIGNID]
		,[NB_U_OUVERTURES]
		,[NB_OUVERTURES]
        
			,code_action)
		 VALUES ('SP_TI_STATS_SCENARIO_KARIBOU'
			   ,S.[DT]
			   ,S.[CAMPAIGNID]
			   ,S.[NB_U_OUVERTURES]
			   ,S.NB_OUVERTURES
               
			   	,S.code_action) 
	WHEN MATCHED
		THEN 
			UPDATE SET T.[NB_U_OUVERTURES] = S.[NB_U_OUVERTURES]
			   ,T.[NB_OUVERTURES]=S.NB_OUVERTURES;
			   
			   
---
-- Nb de cibles/bounces/livraison
MERGE DATA_TI_STATS_DATASTUDIO AS T
USING (SELECT 
	cast(sent_dt as date) as DT,
	T1.campaignid,
	count(*) as NB_CIBLE,
	sum(case when smtperr=250 then 1 else 0 end) as NB_DELIVRE,
	sum(case when smtperr=250 then 0 else 1 end) as NB_BOUNCES,
	T4.code_action
FROM ACTIONQUEUE T1 with (nolock)
INNER JOIN ACTION_TI_KARIBOU_DOC_QUIZZ T2 with (nolock) ON T2.ID = T1.DITEMID
INNER JOIN campaigns T3 with (nolock) ON T3.id=T1.campaignid AND T3.category = 'TI_MKG_SCENARIOS'
INNER JOIN ARTICLES_TI_KARIBOU_SCENARIOS_REF T4 with (nolock) ON T4.id = T2.id_scenario AND T1.dlistid = 434
WHERE sent_dt  >= cast(getdate()-@DELTADAYS  as date) AND sent_dt < cast(getdate() as date)
group by cast(sent_dt as date),T1.campaignid, T4.code_action) AS S
ON (T.DT=S.DT AND T.NOM_STAT='SP_TI_STATS_SCENARIO_KARIBOU' AND T.CAMPAIGNID=S.CAMPAIGNID AND T.code_action = S.code_action)
WHEN NOT MATCHED BY TARGET
	THEN 
	INSERT (NOM_STAT
		,[DT]
		,CAMPAIGNID
		,[NB_CIBLE]
		,[NB_DELIVRE]
		,[NB_BOUNCES]
        
			,code_action)
		 VALUES ('SP_TI_STATS_SCENARIO_KARIBOU'
			   ,S.[DT]
			   ,S.[CAMPAIGNID]
			   ,S.[NB_CIBLE]
			   ,S.[NB_DELIVRE]
			   ,S.NB_BOUNCES
               
			   ,S.code_action) 
	WHEN MATCHED
		THEN 
			UPDATE SET T.[NB_CIBLE] = S.[NB_CIBLE]
			   ,T.[NB_DELIVRE]=S.NB_DELIVRE
			   ,T.[NB_BOUNCES]=S.NB_BOUNCES;






			   -- taux désabo 

MERGE INTO DATA_TI_STATS_DATASTUDIO AS T
USING (
       select cast(T4.sent_dt as date) as DT, t1.campaignid,count(*) AS nb_optout,count(distinct(T1.userid)) AS nb_u_optout,t6.code_action
    FROM flags t1 with (nolock)
inner join actionqueue T4 with (nolock) on T1.inqueueid = T4.inqueueid AND T4.dlistid = 434
INNER JOIN ACTION_TI_KARIBOU_DOC_QUIZZ T5 with (nolock) ON T5.ID = T4.DITEMID 
INNER JOIN ARTICLES_TI_KARIBOU_SCENARIOS_REF T6 with (nolock) ON T6.id = T5.id_scenario 
    inner join CAMPAIGN_ACTIONS t2 with (nolock) on t1.campaignid=t2.campaignid and t1.actionid=t2.actionid
    and t1.probeid>0
    where exists (select 1 from campaigns with (nolock) where campaigns.category = 'TI_MKG_SCENARIOS' 
        AND t1.campaignid=campaigns.id and campaigns.run_dt>=cast(getdate()-@DELTADAYS as date))
    and exists (select 1 from MAIL_PROBES t3 with (nolock) where t2.mailid=t3.mailid and t1.probeid=t3.probeid AND t3.CATEGORY ='OPTOUT')
    group by cast(T4.sent_dt as date),t1.campaignid,t6.code_action
) AS S
ON (T.DT=S.DT AND T.CAMPAIGNID=S.CAMPAIGNID  AND T.NOM_STAT='SP_TI_STATS_SCENARIO_KARIBOU' AND T.code_action=S.code_action) 
WHEN NOT MATCHED BY TARGET
    THEN 
    INSERT (
		nom_stat,
		DT,
	campaignid,
	nb_optout,
	nb_u_optout,
	code_action)
         VALUES (
					 'SP_TI_STATS_SCENARIO_KARIBOU',
			S.DT,
		S.CAMPAIGNID,
		 S.nb_optout,
		 S.nb_u_optout,
		 S.code_action)
    WHEN MATCHED
        THEN 
            UPDATE SET 
        t.nb_optout=S.nb_optout,
        t.nb_u_optout=S.nb_u_optout
;



UPDATE DATA_TI_STATS_DATASTUDIO 
SET NOM_CAMPAGNE=(SELECT name FROM campaigns with (nolock) WHERE campaigns.id=DATA_TI_STATS_DATASTUDIO.campaignid)
WHERE NOM_CAMPAGNE IS NULL AND NOM_STAT='SP_TI_STATS_SCENARIO_KARIBOU';

UPDATE DATA_TI_STATS_DATASTUDIO 
SET START_DT=(SELECT RUN_DT FROM campaigns with (nolock) WHERE campaigns.id=DATA_TI_STATS_DATASTUDIO.campaignid)
WHERE START_DT IS NULL AND NOM_STAT='SP_TI_STATS_SCENARIO_KARIBOU';

			   
			  
END
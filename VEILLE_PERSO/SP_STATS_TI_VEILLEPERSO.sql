AS
BEGIN
SET NOCOUNT ON;

IF OBJECT_ID('dbo.TMP_TI_STATS_VEILLE_PERSO', 'U') IS NOT NULL
BEGIN;
	DROP TABLE dbo.TMP_TI_STATS_VEILLE_PERSO;
END;

-- création table temporaire pour stocker les clics des emails
CREATE TABLE dbo.TMP_TI_STATS_VEILLE_PERSO (
	DATE_CREATION DATE,
	NB INT,
	STAT VARCHAR(50),
	QUALIF VARCHAR(50)
	);

INSERT INTO dbo.TMP_TI_STATS_VEILLE_PERSO (DATE_CREATION,NB,STAT,QUALIF)
SELECT cast (GETDATE() as date) as DATE_CREATION , COUNT(*) AS NB,'NB_USER_VEILLE_PERSO' AS STAT,''AS QUALIF 
FROM USERS_TI_EMAILS WITH (NOLOCK)
WHERE EXISTS (SELECT 1 FROM DATA_TI_NL WITH (NOLOCK) WHERE TITRE_CODE=699 AND STATUT_ABONNEMENT=1 AND USERS_TI_EMAILS.ID = DATA_TI_NL.MAIL_CODE);

--        Nb moyen de tag / abonné
INSERT INTO dbo.TMP_TI_STATS_VEILLE_PERSO (DATE_CREATION,NB,STAT,QUALIF)
SELECT cast (GETDATE() as date) as DATE_CREATION,COUNT(*)/COUNT(DISTINCT(MAIL_CODE)) AS NB,'NB_MOYEN_TAGS_USER' AS STAT,''AS QUALIF 
FROM DATA_TI_THESAURUS_USERS WITH (NOLOCK)
WHERE NOT EXISTS (SELECT 1 FROM USERS_TI_EMAILS WITH (NOLOCK) WHERE TESTUSER=1 AND USERS_TI_EMAILS.ID = DATA_TI_THESAURUS_USERS.MAIL_CODE)


--        Nb qui ont reçu une NL
INSERT INTO dbo.TMP_TI_STATS_VEILLE_PERSO (DATE_CREATION,NB,STAT,QUALIF)
SELECT cast (CREATED_DT as date) as DATE_CREATION, COUNT(USERID) AS NB,'NB_EMAIL_ENVOYE' AS STAT,''AS QUALIF
FROM ACTION_TI_VEILLE_PERSO WITH (NOLOCK)
WHERE created_dt > getdate()-7 and exec_dt is not null and campaignid = 2845
group by cast(CREATED_DT as date);

--        Nb moyen d’élément par mail (total / ress doc / actu)
--Total : 
INSERT INTO dbo.TMP_TI_STATS_VEILLE_PERSO (DATE_CREATION,NB,STAT,QUALIF)
select cast (CREATED_DT as date) as DATE_CREATION, SUM(NB_ARTICLES)/COUNT(DISTINCT USERID) AS NB,'NB_ELEMENT_EMAIL' AS STAT ,''AS QUALIF 
FROM ( select CREATED_DT, USERID,
case 
when INFOCOLL is not null then (len(INFOCOLL) - len(replace(INFOCOLL,',','')))+1
else 0
END AS NB_INFOCOLL,
case 
when ACTU is not null then (len(ACTU) - len(replace(ACTU,',','')))+1
else 0
END AS NB_ACTU,
case 
when INFOCOLL is not null AND ACTU is not null then (len(INFOCOLL) - len(replace(INFOCOLL,',','')))+1+(len(ACTU) - len(replace(ACTU,',','')))+1
when INFOCOLL is null AND ACTU is not null then  len(replace(ACTU,',',''))+1
when INFOCOLL is not null AND ACTU is null then (len(INFOCOLL) - len(replace(INFOCOLL,',','')))+1
else 0
END AS NB_ARTICLES
 from ACTION_TI_VEILLE_PERSO
where created_dt >= getdate()-7) T1
group by cast (CREATED_DT as date);



--Ressources docs
INSERT INTO dbo.TMP_TI_STATS_VEILLE_PERSO (DATE_CREATION,NB,STAT,QUALIF)
select cast (CREATED_DT as date) as DATE_CREATION, SUM(NB_INFOCOLL)/COUNT(DISTINCT USERID) AS NB,'NB_ELEMENT_EMAIL_DOCS' AS STAT ,''AS QUALIF 
FROM (select CREATED_DT, USERID,
case 
when INFOCOLL is not null then (len(INFOCOLL) - len(replace(INFOCOLL,',','')))+1
else 0
END AS NB_INFOCOLL,
case 
when ACTU is not null then (len(ACTU) - len(replace(ACTU,',','')))+1
else 0
END AS NB_ACTU,
case 
when INFOCOLL is not null AND ACTU is not null then (len(INFOCOLL) - len(replace(INFOCOLL,',','')))+1+(len(ACTU) - len(replace(ACTU,',','')))+1
when INFOCOLL is null AND ACTU is not null then  len(replace(ACTU,',',''))+1
when INFOCOLL is not null AND ACTU is null then (len(INFOCOLL) - len(replace(INFOCOLL,',','')))+1
else 0
END AS NB_ARTICLES
 from ACTION_TI_VEILLE_PERSO
where created_dt > getdate()-7) T1
group by cast(CREATED_DT as date);





--Actu : 
INSERT INTO dbo.TMP_TI_STATS_VEILLE_PERSO (DATE_CREATION,NB,STAT,QUALIF)
select cast(CREATED_DT as date) as DATE_CREATION, SUM(NB_ACTU)/COUNT(DISTINCT USERID) AS NB,'NB_ELEMENT_EMAIL_DOCS' AS STAT ,''AS QUALIF 
FROM (select CREATED_DT, USERID,
case 
when INFOCOLL is not null then (len(INFOCOLL) - len(replace(INFOCOLL,',','')))+1
else 0
END AS NB_INFOCOLL,
case 
when ACTU is not null then (len(ACTU) - len(replace(ACTU,',','')))+1
else 0
END AS NB_ACTU,
case 
when INFOCOLL is not null AND ACTU is not null then (len(INFOCOLL) - len(replace(INFOCOLL,',','')))+1+(len(ACTU) - len(replace(ACTU,',','')))+1
when INFOCOLL is null AND ACTU is not null then  len(replace(ACTU,',',''))+1
when INFOCOLL is not null AND ACTU is null then (len(INFOCOLL) - len(replace(INFOCOLL,',','')))+1
else 0
END AS NB_ARTICLES
 from ACTION_TI_VEILLE_PERSO
where created_dt > getdate()-7) T1
group by cast(CREATED_DT as date);




---        Nb d’élément par mail (total / actu / ress doc) / compte (nb) d’email concerné


-- répartition nb emails par nb article infocoll
INSERT INTO dbo.TMP_TI_STATS_VEILLE_PERSO (DATE_CREATION,NB,STAT,QUALIF)
select cast (CREATED_DT as date) as DATE_CREATION, COUNT(DISTINCT USERID)as NB,'REPARTITION_EMAIL_INFOCOLL' AS STAT, NB_INFOCOLL AS QUALIF 
FROM (select CREATED_DT, USERID,
case 
when INFOCOLL is not null then (len(INFOCOLL) - len(replace(INFOCOLL,',','')))+1
else 0
END AS NB_INFOCOLL,
case 
when ACTU is not null then (len(ACTU) - len(replace(ACTU,',','')))+1
else 0
END AS NB_ACTU,
case 
when INFOCOLL is not null AND ACTU is not null then (len(INFOCOLL) - len(replace(INFOCOLL,',','')))+1+(len(ACTU) - len(replace(ACTU,',','')))+1
when INFOCOLL is null AND ACTU is not null then  len(replace(ACTU,',',''))+1
when INFOCOLL is not null AND ACTU is null then (len(INFOCOLL) - len(replace(INFOCOLL,',','')))+1
else 0
END AS NB_ARTICLES
 from ACTION_TI_VEILLE_PERSO
where created_dt > getdate()-7) T1
group by cast(CREATED_DT as date) , NB_INFOCOLL;

-- répartition nb emails par nb article actu
INSERT INTO dbo.TMP_TI_STATS_VEILLE_PERSO (DATE_CREATION,NB,STAT,QUALIF)
select cast (CREATED_DT as date) as DATE_CREATION, COUNT(DISTINCT USERID)as NB,'REPARTITION_EMAIL_ACTU' AS STAT, NB_ACTU AS QUALIF 
FROM (select CREATED_DT, USERID,
case 
when INFOCOLL is not null then (len(INFOCOLL) - len(replace(INFOCOLL,',','')))+1
else 0
END AS NB_INFOCOLL,
case 
when ACTU is not null then (len(ACTU) - len(replace(ACTU,',','')))+1
else 0
END AS NB_ACTU,
case 
when INFOCOLL is not null AND ACTU is not null then (len(INFOCOLL) - len(replace(INFOCOLL,',','')))+1+(len(ACTU) - len(replace(ACTU,',','')))+1
when INFOCOLL is null AND ACTU is not null then  len(replace(ACTU,',',''))+1
when INFOCOLL is not null AND ACTU is null then (len(INFOCOLL) - len(replace(INFOCOLL,',','')))+1
else 0
END AS NB_ARTICLES
 from ACTION_TI_VEILLE_PERSO
where created_dt > getdate()-7) T1
GROUP BY NB_ACTU , cast(CREATED_DT as date)

-- réparition nb email par nb article tout confondu
INSERT INTO dbo.TMP_TI_STATS_VEILLE_PERSO (DATE_CREATION,NB,STAT,QUALIF)
select cast (CREATED_DT as date) as DATE_CREATION, COUNT(DISTINCT USERID)as NB,'REPARTITION_EMAIL' AS STAT, NB_ARTICLES AS QUALIF 
FROM (select CREATED_DT, USERID,
case 
when INFOCOLL is not null then (len(INFOCOLL) - len(replace(INFOCOLL,',','')))+1
else 0
END AS NB_INFOCOLL,
case 
when ACTU is not null then (len(ACTU) - len(replace(ACTU,',','')))+1
else 0
END AS NB_ACTU,
case 
when INFOCOLL is not null AND ACTU is not null then (len(INFOCOLL) - len(replace(INFOCOLL,',','')))+1+(len(ACTU) - len(replace(ACTU,',','')))+1
when INFOCOLL is null AND ACTU is not null then  len(replace(ACTU,',',''))+1
when INFOCOLL is not null AND ACTU is null then (len(INFOCOLL) - len(replace(INFOCOLL,',','')))+1
else 0
END AS NB_ARTICLES
 from ACTION_TI_VEILLE_PERSO
where created_dt > getdate()-7) T1
GROUP BY NB_ARTICLES, cast(CREATED_DT as date)

END
AS
BEGIN
SET NOCOUNT ON;
-- création de la table temporaire pour stocker les ID de la table USERS_TI_SUSPECT qui sont à purger



-- remplissage des contacts à purger 
-- /!\ requête temporaire à adapter
INSERT INTO dbo.TMP_TI_SUSPECT_PURGE (USERID,PURGE_DT,ID_SUSPECT)
SELECT ID,GETDATE(),MAIL FROM USERS_TI_SUSPECT 
WHERE OPTOUT=999 AND (DERNIERE_ACTIVITE_DT < DATEADD(yy,-3,GETDATE())) ;


 
-----------------------------------------------------------
------- SUPPRESSION DES DONNEES LIEES AU CONTACT ---------- 
-----------------------------------------------------------

--Suppression des taxonmie
DELETE T1 FROM TAXONOMY_L49_T4 AS T1
WHERE EXISTS (SELECT 1 FROM TMP_TI_SUSPECT_PURGE TP WHERE TP.USERID = T1.USERID);

-- Suppression des INFOSPEC
 DELETE T1 FROM DATA_TI_SUS_INFOSPEC AS T1
 WHERE EXISTS (SELECT 1 FROM TMP_TI_SUSPECT_PURGE TP WHERE TP.USERID = T1.SUSPECT_ID);
 
 

 -- SUPPRESSION DU SUSPECT  
DELETE T1 FROM USERS_TI_SUSPECT AS T1
WHERE EXISTS (SELECT 1 FROM TMP_TI_SUSPECT_PURGE TP WHERE TP.USERID = T1.ID);
 
  
 

 

 END